---
author: "James"
categories: ["skit"]
date: 2015-10-28T19:54:34Z
description: ""
draft: false
slug: "first-aid-demonstration"
tags: ["skit"]
title: "First Aid Demonstration"

---

Cast: Narrator, Victim, Scout 1, Scout 2

N: For tonight’s first aid demonstration, we’ll walk through a scenario. You were meandering through the woods, looking for things to set on fire in a violent and spectacular (yet safe) fashion when you came across this unconscious individual. He’s clearly probably injured! First, establish visual confirmation that the situation is safe.

S1: A body?  
S2: Is he dead? What killed him?  
S1: Was it a snake? I hate snakes.  
S2: Where is it?  
S1: It could be anywhere!  

N: Next, cautiously approach the body and diagnose the symptoms.

S1: Is he breathing? HE’S NOT BREATHING! Oh, wait, never mind.  
V: _[Gentle snoring]_  
S2: His breath is slow and raspy. I think he’s having trouble holding on.  
V: _[Snore turns into a cough, then back to snoring. Mouth open slightly.]_  
S1: He has fluid coming out of his mouth. IT’S BLOOD!  
S2: That’s drool.  
S1: _[Looking into victims mouth]_ I SEE EXPOSED BONE!  
S2: Those are teeth.

N: Comfort and reassure the victim.

S1: It’s ok, buddy. We’ll get you out of this. Everything is going to be fine.  
V: _[Snoring]_

N: Finally, begin CPR.

S1: _[Puts face close to victim and takes a deep, loud breath]_  
V: Wait, WHAT?! I did not sign up for this, I’m outta here!

---
author: "James"
categories: ["skit"]
date: 2015-10-28T20:38:28Z
description: ""
draft: false
slug: "summer-camp-dictator"
tags: ["skit"]
title: "Summer Camp Dictator"

---

Cast: Sam, Loud person, Leader, Crazy Eddie, Small group of trailmen

SAM: Fall in! I said fall in! Alright, you maggots! I'm Sam, and I'm your acting First Officer for camp this week! You may refer to me as "Dictator."

LOUD PERSON: Mr. Dictator?...

SAM: Silence! I wasn't done yet. In fact, I haven't even started! Now, you worthless bunch of uniformed slobs may think that you came here this week to learn a bunch of boring knots and lashings. Wrong! In fact, you are really here to blow stuff up and help me take over the world!

_[Cheering]_

_[Leader beckons to Sam and says something to him. Sam returns to the group]_
 
SAM: Well, ok. It seems that you are here for knots and lashings after all. [Starts shouting] But you will also earn your Fire Ranger badge! And it will be instructed by Crazy Eddie!
 
_[CRAZY EDDIE laughs maniacally.]_
 
LOUD PERSON: We're all gonna die!
 
_[LEADER glares at Sam]_
 
SAM _[In a monotone voice]_: And that training will be performed at an approved fire ring with leader supervision and no explosives.
 
LOUD PERSON: Will there be upbeat song and dance numbers?
 
SAM: That would imply good taste! Where do you think you are, trailman? This is a skit!
 
LOUD PERSON: The menu says "Grub" for like the entire week.
 
SAM: Do you have a problem with that, trailman?! Now, be silent as I read the list of available trail badges this week. We have:

- Cooking things
- Shooting things
- Cooking things that have been shot
- Knots and Lashings
- Not Lashings
- Lighting things on fire.
- And blowing things up.
 
_[LEADER coughs]_
 
SAM: One moment, please.
 
LOUD PERSON: Look out! It's Crazy Eddie!
 
SAM: Huh? What fool gave him matches?! Scatter like rabbits people, like rabbits!


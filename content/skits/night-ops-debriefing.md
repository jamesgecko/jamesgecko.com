---
author: "James"
categories: ["skit"]
date: 2015-10-28T21:43:53Z
description: ""
draft: false
slug: "night-ops-debriefing"
tags: ["skit"]
title: "Night Ops Debriefing"

---

Editor's Note: _Night Ops_ is an annual event in North East Georgia. http://nightopsadventures.com/

Cast:

* Several trailmen in sleeping bags under a tarp
* Patrol leader offstage
* First Officer

FIRST: Camp, report! Has anyone seen Hamster patrol?  
_[PL crawls in, tired and out of breath]_  
PL: Hamster patrol reporting, sir!  
FIRST: Where have you been?!  
PL: Operating nights, sir.  
FIRST: Buddy system! Where are your patrol members?  
PL: I lost a lot of good men tonight.  
_[PL yanks tarp off, revealing sleeping bags]_  
PL: Look at all those body bags. I’m the only one left standing!  
FIRST: You mean they’re… dead?  
PL: What? No! They were up all night; they’re sleeping.  
FIRST: But they’ll be OK?  
PL: I don’t know. I just don’t know. The P.T.S.S.D. is already setting in.  
FIRST: Don’t you mean P.T.S.D.? Post-traumatic stress disorder?  
PL: Of course not! Post-traumatic super-soaker disorder.  
_[One of the trailmen in sleeping bag suddenly sits bolt upright and starts yelling]_  
Trailman: They’re shooting! It’s wet! Why are there water guns? Whhhhhyyy?!  
_[Trailman flops back down.]_  
FIRST: I want a full report.  
PL: There we were. The night started out fine. We only got lost once. We braved the crazy skill stations. But then it happened.  
FIRST: The super soakers?  
PL: The girls. They beat us at the fire building contest! That’s when it went downhill.  
FIRST: That’s impossible! Your patrol had Crazy Eddie! No man alive could torch random objects as fast as he could.  
PL: But they were not men!  
FIRST: By the way, where is Crazy Eddie?  
PL: He got sent home for torching random objects faster than any man alive.  
FIRST: When did that happen?  
PL: So there we were, the forest ablaze around us. Our only hope was to make it to the lake before the roaring fire cut off our escape route. Eddie was laughing maniacally.  
FIRST: I don’t remember a forest fire last night at all.  
PL: You weren’t there, man!  
FIRST: Wait, let me guess. The girls patrol put out the actually-not-very-large fire and you guys got lost while running to the next station.  
PL: That sir, is a lie. A complete fabrication! Slander! Outrageous! We were only walking very quickly, not running.  
FIRST: And then you got lost.  
PL: There we were, deep in the heart of the jungle. It has been three hours since we last saw a sign of civilization. Some thought that we might not make it the precious few hours until dawn. Harold had eaten all our snacks and Crazy Eddie had exhausted all his fire making equipment. We were on the verge of starvation and death!  
FIRST: Then the girls patrol found you.  
PL: I don’t want to talk about this anymore.  

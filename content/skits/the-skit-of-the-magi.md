---
author: "James"
categories: ["skit"]
date: 2015-10-28T21:29:25Z
description: ""
draft: false
slug: "the-skit-of-the-magi"
tags: ["skit"]
title: "The Skit of the Magi"

---

Cast: Wiseman Alpha, Wiseman Delta, Wiseman Zeta, and Narrator (may be one of the wisemen).

NARRATOR: The Shepards made it to Bethlehem the night Jesus was born. The Magi didn’t show up until almost two years later. Why? Obviously, they were time travelers.

_[Wise men on stage, all wearing sunglasses]_  
Z: Wiseman Alpha, what is the status of our current position?  
A: Lost, Wiseman Zeta.  
Z: Delta, where is our map?  
D: Eaten by a camel, sir.  
Z: No matter, I’ll just use my device from the future to locate our current position... It says “GPS cannot find satellites.”  
A: I told you no good could come of time travel  
D: You lost our camels in the space-time continuum!  
Z: Maybe if I was to just go forward _briefly_ and bring back a satellite...  
Z & A: NO!  
A: I’m hungry.  
Z: Our food was on the camels. It’s a good thing we saved our gifts.  
A: I wonder what Myrrh tastes like.  
D: They use that for making mummies, don’t they?  
A: Frankincense looks like dried fruit...  
D: Eww.  
Z _[pointing]_: Great Scott! There’s the star again!  
A: How did we miss it?  
Z: ...Because we were walking the _opposite direction_.  
A: And there’s a palace! They keep kings in those, don’t they? We’re looking for a king.  
D: Isn’t the current king a mass murder or something?  
Z: I see no way in which this plan could fail.  
A: I’m sure it will be fine.  
A, D & Z: Onward!

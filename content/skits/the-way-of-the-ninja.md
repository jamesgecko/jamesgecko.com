---
author: "James"
categories: ["skit"]
date: 2015-10-28T21:03:05Z
description: ""
draft: false
slug: "the-way-of-the-ninja"
tags: ["skit"]
title: "The Way of the Ninja"

---

Cast: Professor, Ninja, Victim 1 (not wearing shoes), Victim 2 (not wearing shoes)

_[The professor walks onto stage and begins to speak]_

Hello. You are all doubtless here today to learn about ninjas. Unfortunately, the way of the ninja is fraught with hardness-es. These include:

- Exercise
- Assassinating people
- Sneaking around
- Getting killed... _multiple times!_

If any of this appeals to you, you are in the right place. Now, the first most important point: Socks. Some people claim that ninjas don’t care about socks. Those people would be wrong.

The next most important thing to remember is how to become one with the shadows. Fortunately, this is highly overrated. People are very unobservant.

However, all this lecture would be pointless and boring if there was to be no demonstration. Observe as this trained ninja infiltrates a campsite of hapless trailmen.

_[Cut to the two victims sitting around a campfire]_  
_[NINJA is slipping up behind VICTIM 1]_  
VICTIM 1: So then, the frumious Bandersnatch grabbed Harold and-  
VICTIM 2 _[pointing]_: What’s that?!  
_[NINJA grabs VICTIM 1]_  
_[A brief struggle ensues]_  

PROFESSOR: Observe how the skilled ninja goes directly for the sock.  
NINJA: Behold! I have prevailed over my formally sock wearing foe!  
PROFESSOR: This will conclude our demonstration. Thank you.

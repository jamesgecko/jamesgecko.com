---
author: "James"
categories: ["skit"]
date: 2015-10-28T21:10:25Z
description: ""
draft: false
slug: "nature-watch"
tags: ["skit"]
title: "Nature Watch"

---

Cast:

- Narrator
- Animal(s). Acts out what the narrator is saying.
- Fearsome automobile. Makes car noises and runs over animals.
- People to mob the narrator. Reusing the actors playing the automobile and animals may be easiest.

NARRATOR: Hello, and welcome to nature watch. Tonight we'll be looking at common animals found in an urban environment. The first is the opossum. Subsiding mostly off plants and insects, do not be fooled within its gaping maw lies row upon row of sharp pointy teeth! When threatened it's only natural inclination is to play dead. Unfortunately, it's only natural predator is the fearsome automobile.

AUTO: Vroom! Vroom!
_[POSSUM is struck by AUTO and pretends to be dead]_

Next up is the deer. Males grow majestic and antlers. Although it subsists off plants, do not be fooled. Beyond it's gaping maw lie millions of sharp, pointy teeth! When threatened it will freeze for 30 seconds and then bolt. Unfortunately this is slower than its natural predator: the fearsome automobile.

AUTO: Vroom! Vroom!
_[DEER is struck by AUTO and pretends to be dead]_

Finally there is the Majestic Trailmen. They eat mostly grains, vegetables and as much junk food as they can scavenge. When threatened, their natural defense is to run like crazy. Beware! They often travel in packs. Oh no! They've seen me.

_[NARRATOR is mobbed by TRAILMEN]_

NARRATOR: Tune in next week for our documentary: "Bigfoot: fact?... Or Troopmaster?"


---
author: "James"
categories: ["skit"]
date: 2015-10-28T21:22:34Z
description: ""
draft: false
slug: "lost-in-the-woods"
tags: ["skit"]
title: "Lost in the Woods"

---

**Cast**: A Fearless TRAILMAN, A SHAKEspeare lover, Gollum, a BEAR, and a car DRIVER. (BEAR and DRIVER may be played by the same person)

_[The party walks to the center of the stage and stops. SHAKE is wearing a backpack]_  
_[SHAKE strikes a dramatic pose]_  
SHAKE: Alas! We are lost in the woods!  
TRAILMAN: Be quiet! You aren't helping things any. You ate all our food, Gollum ate our map...  
GOLLUM: Stupid trailmans! Hates them both! Hates them all!  
SHAKE: Are we there yet? Mine feet hath become as stones.  
GOLLUM: Stupid rock-footed trailmans.  
TRAILMAN: Fine. We'll stop.  
_[SHAKE sets down their backpack. On GOLLUM's foot]_  
GOLLUM: Grah! Our foots! They hurts, they does!  
_[GOLLUM kicks SHAKE's backpack]_  
SHAKE: Knowest thou not? That knapsack contains fine china!  
_[SHAKE opens their backpack and pulls out a broken cup]_  
SHAKE: Alas, my mother's dinnerware. I knew thee well.  
TRAILMAN: Seriously. Get a mess kit already!  
SHAKE: No need. We will all starve and die!  
TRAILMAN: Look! A bear!  
SHAKE: We will all die without starving!  
GOLLUM: Graaaaah!  
_[GOLLUM tackles the BEAR, wrestles it to the ground, and starts punching it]_  
TRAILMAN: You killed it!  
SHAKE: It is dead. Huzzah! We will now continue to starve to death.  
GOLLUM: Food!  
SHAKE: Food?  
TRAILMAN: Of course! With his highly developed survival skills, Gollum can gut the bear, prepare the meat, and get us enough food to get back to camp with!  
SHAKE: He is pulling guts out of the bear and eating them like spaghetti.  
TRAILMAN: Ewww. Stop that!  
GOLLUM: Grrrr. Gollum's bear! Gollum's food! His! Mine!  
_[SHAKE strikes a dramatic pose again]_  
_[BEAR crawls off inconspicuously at some point here]_  
SHAKE: Never mind what I said earlier. We will starve!  
TRAILMAN: Wait! Whats that?  
_[GOLLUM sniffs the air like a dog and growls]_  
SHAKE: It appears to be some sort of marvelous transportation device completely unrelated to the aforementioned bear.  
TRAILMAN: It's a car! We're saved! Try to signal to it! Make it stop!  
_[SHAKE does the hitchhiking symbol. GOLLUM tries to make it honk.]_  
DRIVER: Hello!  
GOLLUM: Grrr...  
DRIVER _[Backing away]_: Is that thing trained?  
TRAILMAN: No, no, it's perfectly alright.  
_[GOLLUM growls]_  
_[GOLLUM chases DRIVER offstage. TRAILMAN and SHAKE follow, trying to stop DRIVER]_  

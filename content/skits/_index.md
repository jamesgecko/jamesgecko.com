---
date: 2015-10-28T20:05:57Z
draft: false
slug: "skits"
title: "Skitastrophe"
footer: "These skits are licensed under a <a rel='license' href='http://creativecommons.org/licenses/by-nc-sa/4.0/'>Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>. If you're just performing them at a meeting or campfire, you don't need to worry about it. :-)"
---

This is a collection of original skits suitable for adaption and performance at Trail Life, American Heritage Girl, and Boy Scout events.

Note that some of these skits mention Trail Life ranks and badges. Customization of the names and terminology for your unit is encouraged!

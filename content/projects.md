---
author: "James"
date: 2015-09-26T02:44:12Z
description: ""
draft: false
slug: "projects"
title: "Side Projects"

---

Sometimes when I'm not writing job code, I write personal code! Not pictured: dozens of totally useless unfinished utilities and games.

#### Save Central
PC video games put save files _all over the place_. This utility moves them to the default Windows `%UserProfile%\Save Games` folder and creates invisible junctions back to their original locations.
[GitHub repo](https://github.com/jamesgecko/save-central)

#### Lazy Timesheets
This correlates Toggl entries with Pivotal Tracker stories to make it easier to generate pretty timesheets.
[GitHub repo](https://github.com/jamesgecko/lazytimesheets)

#### Lyrical
This is really basic lyrics projection software designed to aid churches with their worship services. Technically mostly functional, but not pretty and very unfinished.
[GitHub repo](https://github.com/jamesgecko/Lyrical)


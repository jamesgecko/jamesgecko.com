---
author: "James"
date: 2017-03-19T17:17:05Z
description: ""
draft: false
slug: "gamer-tags"
title: "Gamer Tags"

---

For the convenience of friends.

- Discord: James#9071
- Steam: [jamesgecko](http://steamcommunity.com/id/jamesgecko)  
- GOG.com: [JamesGecko](https://www.gog.com/u/JamesGecko/games)
- Battle.net: JamesGecko#1452
  - [Overwatch](https://www.overbuff.com/players/pc/JamesGecko-1452)
  - [Heroes of the Storm](https://www.hotslogs.com/Player/Profile?PlayerID=802767)
- PSN (PS3): [JamesGecko_](http://psnprofiles.com/JamesGecko_)
- Xbox Live (PC, 360): [JamesGecko](https://account.xbox.com/en-US/Profile?GamerTag=JamesGecko)
- Nintendo 3DS: 3668-9882-1748
- ~~Nintendo Switch: SW-7168-3975-9293~~ (inactive)


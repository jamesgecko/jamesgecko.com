---
author: "James"
date: 2018-03-14T00:45:02Z
description: ""
draft: false
slug: "macos-keyboard-automation"
aliases:
  - /macos-keyboard-automation
title: "MacOS Keyboard Automation"

---

A coworker asked me to summarize the desktop utilities I use. I didn't know I had knowledge anyone wanted! Here's a list of tools I regularly use to bend my environment to my whims. The most important quality in a tool is that it blends into the background and I don't have to think about it. In order of ease of use:

# [MacOS](https://apple.stackexchange.com/a/55294/61990)
First thing is to enable keyboard control of GUI elements. This will enable you to press [tab] to switch between buttons in modal dialogs.

System Preferences → Keyboard → Shortcuts → Full Keyboard Access... → All Controls

# [Alfred](https://www.alfredapp.com/)
Ok, so this is easy. It's like Spotlight. Map it to Cmd+Space. Use it to launch all your applications, lock your computer, search your files, search the internet. Add custom search keywords; I have some set up to search the internal admin dashboard and wiki at work. [DevDocs.io](https://devdocs.io/) is another great custom search to add.

This is the most basic (and free) use. We'll be back.

# [Spectacle](https://www.spectacleapp.com/)
Control windows with keyboard shortcuts.
- Cmd+Alt+Arrow to shift a window to a side of the screen. Multiple times to cycle through window sizes.
- Cmd+Alt+Ctrl+Arrow to shift a window between monitors.
- Alt+Cmd+F for fullscreen.

There are others, but those are the ones I primarily use.

# [Karabiner-Elements](https://pqrs.org/osx/karabiner/)
Now we're getting spicy. Map keys to other keys.
- Swap Cmd and Alt, but only on your Windows keyboard.
- Make Caps Lock behave like Esc when pressed, but Ctrl when pressed with another key.
- Make the `Enter` key open files in finder.
- [Look at all this crazy stuff](https://pqrs.org/osx/karabiner/complex_modifications/).

# Alfred (paid)
Ok, so the paid version of Alfred lets you create elaborate workflows using stuff you type into the launcher as input. Look at what you can build:
- A `meme` command that searches your gifs folder and pastes images into Slack.
- A `menu` command that searches the menus of the active application.
- A completely keyboard driven interface on top of the reminders app.
- A `shrug` command? `¯\_(ツ)_/¯`

# [Hammerspoon](https://www.hammerspoon.org/)
In terms of power and ease of use, this is sort of a massive multipurpose chainsaw that you program using Lua. It slightly overlaps with everything else on this list. You can change the way alt-tab works. You can mute iTunes when your laptop connects to wifi at work. You can make your terminal and IDE always open on the same screens in the same positions.

This is a very deep rabbit hole.

# AppleScript
You're gonna start running into this with Alfred workflows or maybe Hammerspoon pretty quick. It lets you automate basically anything you can do with a mouse and keyboard. Pull up `Script Editor` and poke around. It's very powerful. It's also fiddly and fragile. Applications don't always do what you want them to do instantly, so you end up with lots of code that works like this:
- Click the button
- Wait half a second for search to open
- Type this string and press enter
- Wait half a second for search results to appear
- Click the first result
- Wait half a second

And so on. But it's so useful you'll probably end up using it quite a bit for automating GUI apps.

Yes, you can use Alfred to launch a Hammerspoon macro that calls AppleScript.

# Bash
It's much easier to automate terminal applications than it is UI stuff. Lots of small commands fit together in satisfying, occasionally productive ways. Learn how to use Bash and write scripts. I don't know of a good tutorial, but command line knowledge in general is super useful for a developer.

You may also see references to [Zsh](http://ohmyz.sh/) or [Fish](https://fishshell.com/). Those are good too!

# Vim or Emacs
Learn to edit text using Vim or Emacs. Vim lets you do build tiny macros with text on the fly. Emacs is kind of an application platform that happens to have a text editor in it. If you know the basics of either of these, you'll start wanting to use them everywhere. MacOS lets you use Emacs editing commands in _literally every text box_. I use Chrome's cVim extension to browse the internet using my keyboard.


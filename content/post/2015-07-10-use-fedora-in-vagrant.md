---
author: "James"
date: 2015-07-10T20:35:20Z
description: ""
draft: true
slug: "use-fedora-in-vagrant"
aliases:
  - /use-fedora-in-vagrant
title: "Using Fedora in Vagrant"

---

I use [Vagrant](http://vagrantup.com) to manage my development virtual machines. It defaults to an Ubuntu image. But maybe you prefer Fedora. Where's a good Fedora image? My first stop was [Atlas](https://atlas.hashicorp.com/boxes/search). Unfortunately, the latest and greatest Fedora releases were uploaded by random people, and I have no way of knowing if they're _really_ uploading unmodified images.

But it turns out we can get image directly from Fedora! Ideal. On the [Fedora Cloud Base Images](https://getfedora.org/en/cloud/download/index.html) page, there are links to Vagrant boxes. Copy the appropriate download URL.

Here is the relevant section in my Vagrantfile. Your box_url will probably differ.
```
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "fedora22"
  config.vm.box_url = "https://download.fedoraproject.org/pub/fedora/linux/releases/22/Cloud/x86_64/Images/Fedora-Cloud-Base-Vagrant-22-20150521.x86_64.vagrant-virtualbox.box"
  # ...
end
```

Now, there's a hitch. The image doesn't include the Virtualbox guest additions. We'll need those to forward ports and access the host filesystem. Fortunately, there's a plugin for that. In the same directory as your Vagrantfile, run this:

```
$ vagrant plugin install vagrant-vbguest
```

You're then free to run `vagrant up`. When the box comes up, the plugin will install kernel headers and a C compiler so it can build the guest additions. If your internet connection is as slow as mine is, it'll just complain that `yum` has been deprecated in favor of `dnf` and sit there for a while. It's not frozen; just wait for the package manager output.

After all this was over, I was greeted by the following message:
```
Copy iso file C:\Program Files\Oracle\VirtualBox\VBoxGuestAdditions.iso into the box /tmp/VBoxGuestAdditions.iso             
mount: /dev/loop0 is write-protected, mounting read-only                                                                     
Installing Virtualbox Guest Additions 4.2.16 - guest version is                                                              
Verifying archive integrity... All good.                                                                                     
Uncompressing VirtualBox 4.2.16 Guest Additions for Linux............                                                        
VirtualBox Guest Additions installer                                                                                         
Removing installed version 4.2.16 of VirtualBox Guest Additions...                                                           
Copying additional installer modules ...                                                                                     
Installing additional modules ...                                                                                            
Removing existing VirtualBox non-DKMS kernel modules[  OK  ]                                                                 
Building the VirtualBox Guest Additions kernel modules                                                                       
The make utility was not found. If the following module compilation fails then                                               
this could be the reason and you should try installing it.                                                                   
                                                                                                                             
The gcc utility was not found. If the following module compilation fails then                                                
this could be the reason and you should try installing it.                                                                   
                                                                                                                             
Building the main Guest Additions module[FAILED]                                                                             
(Look at /var/log/vboxadd-install.log to find out what went wrong)                                                           
Doing non-kernel setup of the Guest Additions[  OK  ]                                                                        
Installing the Window System drivers[FAILED]                                                                                 
(Could not find the X.Org or XFree86 Window System.)                                                                         
An error occurred during installation of VirtualBox Guest Additions 4.2.16. Some functionality may not work as intended.     
In most cases it is OK that the "Window System drivers" installation failed.                                                 
[default] No guest additions were detected on the base box for this VM! Guest                                                
additions are required for forwarded ports, shared folders, host only                                                        
networking, and more. If SSH fails on this machine, please install                                                           
the guest additions and repackage the box to continue.                                                                       
                                                                                                                             
This is not an error message; everything may continue to work properly,                                                      
in which case you may ignore this message.                                                                                   
[default] Mounting shared folders...                                                                                         
[default] -- /vagrant                                                                                                        
The following SSH command responded with a non-zero exit status.                                                             
Vagrant assumes that this means the command failed!                                                                          
                                                                                                                             
mount -t vboxsf -o uid=`id -u vagrant`,gid=`id -g vagrant` /vagrant /vagrant                                                 
                                                                                                                             
Stdout from the command:                                                                                                     
                                                                                                                             
                                                                                                                             
                                                                                                                             
Stderr from the command:                                                                                                     
                                                                                                                             
/sbin/mount.vboxsf: mounting failed with the error: No such device                                                           

```

This is problematic! Here's what I found in the log:

```
/tmp/vbox.0/r0drv/linux/memobj-r0drv-linux.c: In function ‘rtR0MemObjNativeMapUser’:
/tmp/vbox.0/r0drv/linux/memobj-r0drv-linux.c:1539:26: error: ‘struct mm_struct’ has no member named ‘numa_next_reset’
                 pTask->mm->numa_next_reset = jiffies + 0x7fffffffffffffffUL;
                          ^
scripts/Makefile.build:258: recipe for target '/tmp/vbox.0/r0drv/linux/memobj-r0drv-linux.o' failed
make[2]: *** [/tmp/vbox.0/r0drv/linux/memobj-r0drv-linux.o] Error 1
Makefile:1394: recipe for target '_module_/tmp/vbox.0' failed
make[1]: *** [_module_/tmp/vbox.0] Error 2
/tmp/vbox.0/Makefile.include.footer:79: recipe for target 'vboxguest' failed
make: *** [vboxguest] Error 2
Creating user for the Guest Additions.
Creating udev rule for the Guest Additions kernel module.
```

Sooo... the guest additions kernel module didn't build. I have no idea how to fix this. I'm using an older version of Vagrant (1.2.7), and there's a new version out now. I'll try upgrading Vagrant, then destroy the box and start over.


---
author: "James"
date: 2021-01-24
slug: "user-tracking-is-unethical"
tags: ["adtech", "ethics"]
title: "User Tracking is Unethical"
---

I saw a discussion about the Facebook tracking pixel recently. The crux of the discussion is that Facebook's tracking is good because it helps small businesses. Now, I work at an advertising methics company. Most of our customers are small businesses. I agree, small business are good.

But cross site user tracking is unethical.

- It's unclear if personalized ads directly lead to sales in many cases. Facebook might be invading user privacy for nothing.
  - [How User Tracking Devalues Ads](http://ignorethecode.net/blog/2020/12/24/how_user_tracking_devalues_ads/)
  - [Freakonomics: Does Advertising Actually Work? (Part 2: Digital)](https://freakonomics.com/podcast/advertising-part-2/)
- US federal agencies are taking advantage of ad tracking data to erode the civil liberties of citizens.
  - [Ars Technica: Secret Service buys location data that would otherwise need a warrant](https://arstechnica.com/tech-policy/2020/08/secret-service-other-agencies-buy-access-to-mobile-phone-location-data/)
- You can't opt out. Tracking isn't even dependent on cookies; modern adtech will create a fingerprint of your browser so they can continue to track you even if you clear your cookies.
  - [EFF: Cover Your Tracks](https://coveryourtracks.eff.org/)
- Deleting your Facebook account won't fix it; you will be tracked even if you don't have a account.
  - [Newsweek: Facebook Is Tracking You Online, Even If You Don't Have an Account](https://www.newsweek.com/facebook-tracking-you-even-if-you-dont-have-account-888699)

## How to avoid being tracked

1. Install the [Firefox](https://www.mozilla.org/en-US/firefox/new/) web browser.
2. Install the [Facebook Container addon](https://addons.mozilla.org/en-US/firefox/addon/facebook-container/). This makes Facebook live in it's own little world in your browser, making it more difficult for it track you with cookies. Fingerprinting could still be used to associate your browsing activity with your Facebook account, though.
3. Install the [Privacy Badger addon](https://privacybadger.org/). This is an addon that blocks trackers. It will gets rid of fingerprinting in most cases, unless you disable the extension. Lots of sites will work slightly differently when you use this because invasive tracking is so deeply ingrained in the modern web.

## What about other browsers?
- Chrome is developed by an adtech company that is willfully [making it difficult to thwart tracking.](https://www.xda-developers.com/google-chrome-manifest-v3-ad-blocker-extension-api/)
  - It is worth mentioning that they are working on anti-fingerprinting measures such as [Privacy Budget](https://github.com/bslassey/privacy-budget), though.
- Safari [isn't bad](https://webkit.org/blog/category/privacy/), but it only runs on Apple platforms and doesn't have containers at the time of writing.
- Brave has a history of fraud and shinanigans. [One](https://github.com/lobsters/lobsters-ansible/issues/45) [Two](https://web.archive.org/web/20181221234541if_/https://twitter.com/tomscott/status/1076160882873380870) [Three](https://davidgerard.co.uk/blockchain/2020/06/06/the-brave-web-browser-is-hijacking-links-and-inserting-affiliate-codes/) [Four](https://blog.archive.today/post/626174398020403200/please-provide-all-the-details-about-how-brave)

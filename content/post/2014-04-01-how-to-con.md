---
author: "James"
date: 2014-04-01T16:00:46Z
description: ""
draft: false
slug: "how-to-con"
aliases:
  - /how-to-con
title: "How to Con: Optimizing Happiness at Your First Convention."

---

### Form a party
Cons are more enjoyable when you have people to enjoy them with. Unfortunately, navigating crowded areas gets difficult in a large party, and people start going every which way in groups of five or more. But large parties are fun! I recommend an optimum party size of two and occasional meetups with everyone else. Parties of three can also work, until the third member of the party gets hung up looking at Magic cards in the dealer's room.

### Expect lines for everything
If you can pick up your con badge early, do it. I stood in the registration line for an hour and a half on Friday morning at Dragon*Con 2013. Lines to see famous actors started forming an hour beforehand and spanned city blocks.


### Cosplay
Keep it simple if you've never done it before. You’re going to be on your feet all day, and your costume has to withstand the wear. Don’t bring a prop that you can’t stow; carrying a staff all the time and not having a free hand can get inconvenient.

### The dealers room
Set yourself a budget. Bring cash; it's fast (which dealers appreciate), and many booths won't accept credit or checks. The best time to get deals is on the last day, because it can be less expensive for dealers to discount items than to ship them back home.

Also, expect that the dealer's room with be the most congested area in the entire con. Really claustrophobic.

### The bag of holding
At least one person (preferably more) should have a backpack. Carry a water bottle. Snacks are optional; water is not. Carry emergency costume repair gear. Be prepared to stow a board game some party member bought before realizing they didn't have a bag.

### Cellphones
At a large con, your cellphone probably won't be very reliable. Put it on vibrate. It will usually be too loud to hear it ring indoors, and if it rings in a panel, everyone will hate you. The internet will be unreliable. Communicate with remote party members via text message, and expect some small percentage of the texts to reach their destination an hour after you send them. Expect your battery to drain like anything. Bring a backup battery or a PowerStick or something if you don’t think it’ll make it through the day. My iPhone 5 had enough battery to carry me through each day at Dragon*Con, but a smartphone with an older battery might not.

### Transportation
Local traffic will probably be crazy. If you can park somewhere else and use public transport to get in, do it. The parking will be cheaper, and you won’t have to deal with traffic. Just make sure you can get back to your vehicle later! One branch of MARTA stops running after 7pm.


---
author: "James"
categories: ["life"]
date: 2017-03-29T02:52:57Z
description: ""
draft: true
image: "/images/2017/03/2017-03-24-18.17.492.jpg"
slug: "bullet-points-for-the-week"
aliases:
  - /bullet-points-for-the-week
tags: ["life"]
title: "Bullet points for the week"


---

### Life
- I've acquired shelving and am experimenting with a new concept of storage which involves not putting everything in stacks on the floor. It has potential.
- God told me good news that I wouldn't have wanted to hear a few months ago. But it's good news! And I'm excited about it because it was unmistakably God.
- It's so nice out, I've started walking up several stops to a plaza to catch the bus. I'm not coughing at close range vehicle exhaust, there are places to sit, and there are more people to watch. Plus: mild exercise induced endorphins! Ahhh.
- It has occurred to me basically every day this year that I need to get more sleep. It's probably messing with my cognitive capabilities. That's why I don't like drinking, so why do I like staying up late? Tonight, I take action.
- I'm breaking out of ruts, pretty much.

### Gaming
- Everything about [the development of Stone Soup](https://www.rockpapershotgun.com/2017/03/23/making-dungeon-crawl-stone-soup-with-253-cooks-and-no-head-chef/) is very interesting, particularly the design philosophy document.
- [Card Thief](http://www.card-thief.com/) is an interesting piece of game design.
Also check out the [thoughts behind the stealth mechanics](http://cardthiefgame.tumblr.com/).
- Mass Effect Andromeda has gotten panned, and it's not polished, but it's more Mass Effect! A thousand stars out of ten, game of the year.

### Reading
- [Dresden Files #4: Summer Knight](https://www.amazon.com/Summer-Knight-Dresden-Jim-Butcher/dp/0451458923)
  - I love Dresden books, but I blow through them so quickly! I've been trying to pace myself by slipping them in between more difficult reads. This time I just needed something light because I've been feeling awful. Looking up!
- [Mistborn #3: The Hero of Ages](https://www.amazon.com/Hero-Ages-Book-Three-Mistborn/dp/0765356147)
  - I got this specific copy autographed by Sanderson for the friend I'm borrowing it from. At DragonCon. Last September. I, uh, should probably finish it already. I've read all the Stormlight Archives books since then and going back is a little rough. It's not bad, but Sanderson has clearly gotten better since it was written.
- [Under the Banner of Heaven: A Story of Violent Faith](https://www.amazon.com/Under-Banner-Heaven-Story-Violent/dp/1400032806)
  - Because of course I'm reading about cults. Sean loaned this to me, and I'm not sure I can finish it. It's important, but distinctly unpleasent. I can't do real-life violence stuff. When I read narrative, it's like I'm watching a film. Reading a terse description of violence is as bad or worse than seeing it on a screen; my imagination is too vivid.
- I kinda want to read more C.S. Lewis non-fiction. Absorbing some of his purely logical thought patterns would help me to talk to people who have blinders up around certain religious concepts, I'm thinking.


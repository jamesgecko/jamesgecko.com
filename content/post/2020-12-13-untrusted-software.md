---
author: "James"
date: 2021-01-03
description: ""
slug: "untrusted-software"
tags: ["software", "ethics"]
title: "Writing Untrusted Software"
---

When I look for new piece of software, I have a set of criteria that extends beyond, "does this perform the function I need." I'll err towards open source software. I like getting it from a source that wouldn't knowingly distribute malware, like a distro repository or (gasp) an App Store. And I like using software that is small and efficient, runs on multiple platforms and allows exporting my data in a standard format.

But recently I've been thinking, if this is how I decide what software to trust, how can I build software that requires less trust in me?

Open source is a good start, but it's insufficient. Even if the source code is available, you can't know that what I have uploaded to my git repository is the exact same code that I compiled. Reproducable builds are good, but the average end user is just going to click and download. And if you're going to compile your own binary each time, why even use my binary?

So sandboxed platforms are the next thing to consider. UWP on Windows and App Store on macOS, plus mobile apps on iOS and Android. These have the bonus that a trusted third party has reviewed your software, but submissions on multiple platforms can be a lot of work for a single person. My next thought was WASM, but it doesn't even have a framebuffer standard yet. So we come to browsers. They're sort of the only game in town with such a widely available runtime. Plus they run on everything. I'm just going to sort of assume that browser sandboxes are relatively secure, because at some point you have to trust something.

I really like _personal_ computing; I like having folders of application installers that won't vanish if a website goes offline forever. Maybe one day [Web Bundles](https://web.dev/web-bundles/) will provide that experience for web applications, but right now if we want that we'll have to provide our source code for users to back up and compile and host on their own.

Unfortunately, a lot of the capabilities we might want in an application are a bit difficult in the browser. More experimental APIs like those in [Project Fugu](https://www.chromium.org/teams/web-capabilities-fugu) should close the gap a little bit, soon providing safe-yet-convenient limited filesystem access and the like. And of course, a lot of these capabilities will likely hit Chrome first, with Firefox and Safari following within a year or two. I don't love the latency, or that Google has so much say over web standards, but they are doing the most effort to turn the web into the precise sort of platform I think I would need to write untrusted software.

Finally, there's always the option to ship both an "untrusted" web platform version and source for the user or their platform distributor to build a binary version. Dart/Flutter and Haxe both seem great candidates for this. Providing a web version gives you the option of providing a "try before you install" type experiance for software that doesn't require a lot of platform capabilities. It also allows less tech savvy users to use your software without worrying about how to install it.

---
author: "James"
date: 2016-07-09T03:22:38Z
description: ""
draft: false
cover: "/images/2016/thermaltake.jpg"
slug: "thermaltake-talon-blu-the-missing-manual"
aliases:
  - /thermaltake-talon-blu-the-missing-manual
tags: ["gadget", "documentation"]
title: "Thermaltake Talon Blu FAQ"

---

I like the [Thermaltake Talon Blu](http://www.ttesports.com/productPage.aspx?p=155&g=ftr) mouse, but the manual is lacking. Here are some questions you might have.

## Where can I download drivers or a control panel?
There are none. It's plug and play, the way all input devices should be.

## How do I turn off the glowing light?
Hold down the left mouse button and press the DPI button (next to the scroll wheel). It cycles between on, breathing, and off.

## Is there a way to make the DPI button cycle between two DPIs, not five?
No, there is not. The DPI button is useless for on-the-fly adjustments in the middle of a game.

## I removed the screws, but I can't get the removable side panels off
They come off vertically, not horizontally.

## Can I remove the weights?
Contrary to what the manual says, you cannot.

## No, seriously, how do I get the weights out?
Prepare to void your warranty and maybe screw up your teflon!

1. Take off the side panels. If you look carefully through the clear plastic, you can see the weights.
2. There's a large bit of teflon at the bottom of the mouse, next to the serial code. Between that teflon and the plastic of the mouse is a bit of something like double sided tape. Very carefully pry the tape up with the teflon.
3. There is nothing of interest under the teflon next to the cord. Don't bother prying it up.
4. Under the tape are two screws. Remove them.
5. The bottom of the mouse can now slide back.
6. Remove the weights.
7. Carefully slide the bottom of the mouse back in. There are two tabs near the front. Getting them to line up is a pain; the scroll wheel gets in the way.
8. Put those screws back in.
9. Put the teflon back on. It won't lie flat quite the same way, but I suspect it'll be fine over time.

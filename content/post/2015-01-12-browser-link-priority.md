---
author: "James"
date: 2015-01-12T01:45:31Z
description: ""
draft: false
slug: "browser-link-priority"
aliases:
  - /browser-link-priority
title: "Browser Link Priority"


---

I use keyboard browsing extensions in my browsers. [Vimium](https://vimium.github.io/) in Chrome, and [Pentadactyl](http://5digits.org/pentadactyl/) in Firefox. They emulate shortcuts I'm used to in Vim, and allow me to browse the internet without touching my mouse, for the most part. The core functionality is that whenever I press the `f` key, every link gets a bubble of text next to it. I can type the text in one of the bubbles to "click" on the link. Here, have a thousand words.

![A thousand words](/images/2015/Jan/press-f.png)

Pretty spiffy, eh? Too bad that most sites are a huge mess of confusing junk when you press `f`. Let's look at a forum<sup><a href="#fn1" id="ref1">1</a></sup>.

![Penny Arcade forums](/images/2015/Jan/Screenshot-2014-12-26-18-52-26.png)

For each thread, there are several links:

- The thread title (clicking this takes you to the farthest page you've read in a thread. It's what you want 99% of the time.)
- Up to five page numbers
- The user profile of the person who made the first post
- The first post
- The user profile of the person who made the most recent post
- The most recent post
- Toggle bookmark

Let's press `f`.

![Penny Arcade forums links highlighted](/images/2015/Jan/Screenshot-2014-12-26-18-53-24.png)

When I want to click on a thread title and there are twenty threads visible, there are typically over 150 links highlighted. I don't care about ~85% of those links, and it's easier for me to click on the wrong thing by mistake.

It's an even bigger mess on Reddit; note that the links are so close together that some of the bubbles just overlap.

![Reddit](/images/2015/Jan/reddit-vimium.png)

On websites that have lots of image links close together, sometimes one link can have two bubbles hovering over it, but only one bubble goes to the correct location.

I'm sighted and have the ability to grab the mouse if I need to. I imagine that people using screen readers and other accessibility tools have a much harder time of it.

It would be nice if websites had a way to indicate to the browser that some links are more important than other links. In my fantasy word, this would allow me to only highlight article links when I press `f`. If I pressed `f` again, then all the other links would highlight. Advertisers would hate it, probably. But one can dream.


<sup id="fn1">1. Put aside Penny Arcade hate a moment; I'm not a huge fan of the comic or its creators, but the well moderated forum hosts more or less the best video game community on the internet. <a href="#ref1">↩</a></sup>


---
author: "James"
categories: ["humor", "anime"]
date: 2014-11-10T09:14:27Z
description: ""
draft: false
slug: "how-fatezero-should-have-ended"
aliases:
  - /how-fatezero-should-have-ended
tags: ["humor", "anime"]
title: "How Fate/Zero Should Have Ended"

---

[Fate/Zero](http://myanimelist.net/anime/10087/Fate/Zero) parody/fan fiction. Spoilers, obviously.

Spoilers? Spoilers, spoilers. _Gas leaks!_

- - -

_The scene: As Emiya and Kiri fight to the death, the grail summoning is completed. As the victor of the war, Emiya is pulled into a reality marble. The grail, appearing to him in the form of his dead wife, puts forth it’s glorious plan for world peace._

Emiya spoke to the vision. The shell of his wife, the monstrous grail.  
"That's your plan? Kill everybody? That's a _terrible_ plan."  
"I can only grant wishes for which a solution can be imagined."  
"Creepy lady, even *I* could come up with something better than that. Seriously."  
"The only thing you know, the only thing you have spent your life pursuing, is death and destruction. Could a wish formed from such a mold take any other shape?"  
"Yes, yes it could. Ok, let's kick it down a few notches. No mass murder, no oceans of blood. Toast. I want toast. Can you manage toast without killing anyone?"  
Emiya's vision faded to black. He was on the floor again, surrounded by blood. Kirei looked at him in rage and horror.  
"You fool. What have you DONE?"  
Emiya stood up and wiped blood from his face. His gaze drifted to the side, where the grail had inexplicably landed right-side up. Was that the blood thumping in his ears, or was it making faint ticking noises?  
With a peppy "ding!", a single slice of bread emerged from the depths of the grail and shot four feet into the air. Emiya caught it at the peak of it's ascent with cat-like, time-magic enhanced reflexes. He bit into it with a loud crunch, closing his eyes and enjoying what he had fought so long and hard to achieve: the perfect toast. It was flawless, golden brown and buttery. A low rumbling sounded. A geyser of toast shot out of the grail.  
"You fool!" iterated Kirei redundantly. "It is an instrument of destruction. No wish can be satisfied but that blood is shed. I wholeheartedly approve and congratulate you upon your victory! But did you have to slay us with toast?"

*scene change*

"No! No! Noooooooo!"  
It was horrible. Everywhere, toast-induced death. Houses collapsed under the weight. People smothered in buttery goodness. A body cut in twain by the force with which a slice had issued from it's infernal cornucopia.  
A small child lay in the gutter, in misery. His tiny heart wished for nothing but death. His tiny hand wavered as he slowly reached towards a piece of toast lying next to him.  
"Nooooooo!" yelled Emiya, flinging himself between boy and toast before the child could complete the deed. "Not that. Not ever."  
He was so happy. He had saved someone. But he never ate breakfast again for the rest of his life.


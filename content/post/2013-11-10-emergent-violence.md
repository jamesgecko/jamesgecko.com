---
author: "James"
date: 2013-11-10T22:07:57Z
description: ""
draft: true
slug: "emergent-violence"
aliases:
  - /emergent-violence
title: "Emergent violence"

---

Wii Party U is a little collection of those mini-games Nintendo seems to be so good at pushing out. They're showcases of what can be done with their somewhat unusual hardware.

One of the games is a water carrying relay. It involves setting the tablet controller (the "stream") a ways back from the TV. Participents compete to ferry water back and forth between the "stream" and a "pitcher" (the TV) using "spoons" (Wii remotes). It's an implementation of a fairly boring generic party game.

Once we started playing, however, emergent gameplay appeared, and violence began to escilate. This game is basically Nintendo's take on [B.U.T.T.O.N.](http://brutallyunfairtactics.com/index.html)

The obvious strategy is to bump other people's remotes as you pass to make them loose water. I was somewhat successful with this tactic. I may have bodyslammed another particepant. At this point, another player begain taking a longer, safer route through other rooms in the house. While other people were on their way to the TV, another player hid the stream. People began repositioning the stream every time they got water. Finally, someone absconded with the stream, taking the alternate route to the TV to avoid other players. As they searched for the stream's new hiding place, she stood in front of the TV, scooping and pouring uninterrupted.


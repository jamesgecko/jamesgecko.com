---
author: "James"
categories: ["music"]
date: 2017-03-09T03:57:16Z
description: ""
draft: false
slug: "programming-music"
aliases:
  - /programming-music
tags: ["music"]
title: "Programming Music"

---

For a long time, the only way I could write code was when surrounded by silence. Working in buildings with other people changed that pretty quickly! Random noise (especially speech) still bothers me, a lot. [A lot!](http://hyperboleandahalf.blogspot.com/2010/04/alot-is-better-than-you-at-everything.html) When it gets too much, I try to control it; to put my emotion and focus in that sweet spot where I can ignore the world and solve problems.

So anyway, here are a few of my favorites.

#### [Fastfall (Lifeformed)](http://lifeformed.bandcamp.com/album/fastfall)
The soundtrack to the indie game Dustforce, this is my go to hacking music. It's chill and has a beat; two properties ideal for getting in the zone.

<iframe style="border: 0; width: 400px; height: 120px; clear: both;" src="https://bandcamp.com/EmbeddedPlayer/album=2118918607/size=large/bgcol=ffffff/linkcol=0687f5/artwork=small/transparent=true/" seamless><a href="http://lifeformed.bandcamp.com/album/fastfall">Fastfall by Lifeformed</a></iframe>

<hr style="clear: both;"/>

#### [PROTODOME](http://protodome.bandcamp.com/album/bluescreen)
Chiptune jazz!

<iframe style="border: 0; width: 400px; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=3486767283/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="http://protodome.bandcamp.com/album/bluescreen">BLUESCREEN by PROTODOME</a></iframe>

<hr style="clear: both;"/>

#### [Days Away (Ronald Jenkees)](http://ronaldjenkees.bandcamp.com/album/days-away)
I actually try not to listen to this _too_ frequently because it always kinda blows me away a little when I haven't heard it for a while.

<iframe style="border: 0; width: 400px; height: 120px;" src="http://bandcamp.com/EmbeddedPlayer/album=3884277742/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="http://ronaldjenkees.bandcamp.com/album/days-away">Days Away by Ronald Jenkees</a></iframe>

<hr style="clear: both;"/>

#### [George & Jonathan](http://georgeandjonathan.bandcamp.com/album/beautiful-lifestyle)

This may be more of an acquired taste. Reminds me of Crush 40's game OSTs. I highly recommend listening to album 3 on the extremely neat official [website](http://www.georgeandjonathan.com/). How many bands have 3D chiptune tracker visualizations in their album previews?

<iframe style="border: 0; width: 400px; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=679483997/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="http://georgeandjonathan.bandcamp.com/album/beautiful-lifestyle">Beautiful Lifestyle by George &amp; Jonathan</a></iframe>

<hr style="clear: both;"/>

#### [Ember (Kubbi)](http://kubbi.bandcamp.com/album/ember)
This music always reminds me of The Legend of Zelda for some reason. Just the right mix of melancholy and action.

<iframe style="border: 0; width: 400px; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=631104649/size=large/bgcol=ffffff/linkcol=0687f5/artwork=small/transparent=true/" seamless><a href="http://kubbi.bandcamp.com/album/ember">Ember by Kubbi</a></iframe>

<hr style="clear: both;"/>

#### Misc

I also love Lindsey-Sterling-seeded online radio, but that's a bit difficult to link. I both love and hate Zircon's [Identity Sequence](https://zirconstudios.bandcamp.com/album/identity-sequence). Also, [FantomenK](https://fantomenk.bandcamp.com/track/the-massacre-version-2) is rad. Some early Yellow Magic Orchestra is good; Solid State Survivor is probably their best album for this list. A lot of weird stuff in their discography, though. Note that all this is a bit different than what I'd listen to otherwise.


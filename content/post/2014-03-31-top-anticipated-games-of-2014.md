---
author: "James"
categories: ["games"]
date: 2014-03-31T04:41:54Z
description: ""
draft: false
slug: "top-anticipated-games-of-2014"
aliases:
  - /top-anticipated-games-of-2014
tags: ["games"]
title: "My Top Games of 2014"

---

It's almost April! The perfect time to talk about what games I'm excited about this year. I have limited time to play PC games now, and I'm spending more and more of my leisure time either in front of a mobile device or in the company of friends. Hence, this list is really weird.

### Anticipated
#### Ingress for iOS
[Ingress](https://www.ingress.com/) is great. Geolocation-based territory control battles. An excuse to exercise and interact with random strangers at invisible portals. Unfortunately, it's Android exclusive right now, and I have an iPhone because everything else was terrible last time I was in the market for a smartphone. I've tethered with my Nexus 7 and played a little bit, but setting it up each time and carrying a tablet around while walking for miles is a pain.

#### Super Smash Bros for 3DS
Smash Bros games age really well; I still play Brawl with friends every few weeks, even though I don't have a Wii. I'll play Smash Bros for WiiU, but people will also be playing the 3DS version, so I'll probably need to own that to play with them. I'll also need a 3DS of some sort, but I'll hope for price drops and cross that bridge when I come to it.

#### Baldur's Gate: Enhanced Edition for Android
I've been waiting on this for over a year, and it's _almost here!_ Why yes, I could have picked up the original on GOG.com and beaten it multiple times by now. What are you trying to say?

#### King of New York
This one isn't a video game! What madness? The original King of Tokyo is the finest monster-movie-themed Yahtzee-inspired dice game available. Rumor is that [the sequel](http://boardgamegeek.com/thread/1035073/any-info-on-king-of-new-york-yet/page/1) will add in a board with more than two spaces. It'll be great.

#### Quantum / Android: Netrunner / Flash Duel
These board games didn't even come out this year! But I haven't played them yet, and they look like a lot of fun.

[Quantum](http://www.amazon.com/Passport-Game-Studios-FNFQTMUS01-Quantum/dp/B00GXZUUEM) is an epic 4X space game with simple rules that you can play in forty-five minutes. That doesn't sound too special, but most of the current big titles in this genre take twenty minutes to explain and three to six hours to get though. For some reason it's hard to convince people to play games that take so long. Strange.

[Android: Netrunner](http://www.amazon.com/Android-Netrunner-The-Card-Game/dp/1616614609) is neat because it's an asymmetrical two player card game about bluffing and computer security. It's also neat because the publisher releases new cards in named expansions, not foil packets containing random cards unlike [some games I could mention](/magic-isnt-a-great-game/).

Then there's [Flash Duel](http://www.amazon.com/Sirlin-Games-Flash-Duel-Edition/dp/B0065WWU1Y/). It's really short filler game, but there's a lot of variety, what with characters that play differently and a bunch of different game "modes."

#### Hearthstone for Android
Yes, it's already out on PC. But I can't lie on the couch* while playing Hearthstone, or take my desktop with me for face-to-face games against friends.
* Hearthstone doesn't work very well over remote desktop on a seven inch screen.

#### Codex
Another David Sirlin production, [Codex](http://www.sirlin.net/blog/category/codex) is an RTS-inspired card game. We know next to nothing about it, but it's supposed to be fairly mechanically unique. That, and StarCraft as a card game is something I really want to play. It's been in the works for years; I don't even know if Sirlin will release it in 2014. Optimism, ho!

### Current GOTYs
#### Towerfall Ascension
I converted the common internet computer in the living room into a low-specced Steam Box just for this game. It's insta-glib Super Smash Bros with archers. Matches last for like 30 seconds, tops, but you tend to play a million of them.

#### Threes!
The original sliding-number-addition game. 2048 might be the popular one right now, but it's a ripoff; Threes! is mechanically superior by far. It's good to see that there are still completely new game genres waiting to be discovered.

#### Cardinal Quest 2
Quite possibly the best mobile roguelike. Very satisfying turn-based stealth.

### Want to play more of
#### Star Wars X-Wing Miniatures
I received this as a Christmas present in an online secret santa gift exchange, along with several expansion ships. It's _great_. There are a bunch of incremental rules variations that range from vanilla 1 vs 1 dog-fighting with bare-bones quick-start rules to huge battles full of ships, each with different powers and pilots and load-outs. The problem is that it looks super complicated, and people are dubious when I tell them that it's really not that bad. Look at all those cardboard rulers and markers! It must be terribly technical and involved. I might need to start simple and hide the box containing unused tokens and doodads.


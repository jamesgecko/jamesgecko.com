---
author: "James"
categories: ["linux", "windows"]
date: 2016-10-05T05:14:55Z
description: ""
draft: false
cover: "/images/2016/Explosions1.jpg"
slug: "setting-up-ubuntu-on-windows-10"
aliases:
  - /setting-up-ubuntu-on-windows-10
tags: ["linux", "windows"]
title: "Setting up Ubuntu on Windows 10"

---

You can run Ubuntu on Windows 10. It's not running in a virtual machine. It's _magic_. I'm going to set up my preferred environment. By the end of this document, I'll have fish and Neovim running in tmux with 24-bit colors.

For ease of googling when you inevitably run into an issue, this feature is officially called the "Windows Subsystem for Linux," or WSL for short. You may want to [peruse the FAQ](https://msdn.microsoft.com/en-us/commandline/wsl/faq) before getting started to douse your wild ambitions with cold water.

First, follow the [Microsoft install documentation](https://msdn.microsoft.com/commandline/wsl/install_guide) to get started, then come back.

Done? You now have bash running in a windows terminal. First things first. Your home directory lives in 
`C:\Users\YOUR_WINDOWS_USER_NAME\AppData\Local\lxss\home\YOUR_LINUX_USER_NAME`. If you open Windows Explorer and poke around in `AppData\Local`, you're not going to see an `lxss` folder. That's for a reason. Files in this folder have some extended attributes on them, and you can screw things up by poking at them in the Windows Explorer GUI. This is a command line only zone.

Now. Bash is OK, but I prefer [fish](https://fishshell.com/). Scott Hanselman has a post about [setting that up](http://www.hanselman.com/blog/InstallingFishShellOnUbuntuOnWindows10.aspx). I think Oh My Fish is a bit overkill, but whatever.

Let's take this to the next level and use a half-decent terminal. Enter [ConEmu](https://conemu.github.io/). It has tabs and true color mode. You're going to want the portable version.

You'll want to set up fish as one of the default shells. ConEmu's preferences are sprawling; you'll want to add a new task here:

![](/images/2016/Screenshot-2016-10-04-21.26.41.png)

If you want true color mode, things get trickier. Check out the [ConEmu WSL docs](https://conemu.github.io/en/BashOnWindows.html).

Basically, there's a `wsl` directory bundled with ConEmu. Run the `wsl-con.cmd` command in there, and it'll download a bunch of stuff into the directory (That's why you're using portable ConEmu). Then drop the `wsl-con.bat` and `boot.sh` scripts in and make a ConEmu task that points at them. Use `/dir "c:\your\path\to\wsl\"` in the task parameters field to set the working directory. I had to add the directory containing `ConEmuC.exe` to my Windows `%path%`. It's not perfect; I get an extra tab that says `ConEmuC: Root process was alive less than 10 sec, ExitCode=0 Press Enter or Esc to close console...` every time I open a new fish tab.
- - -
[Installing Neovim from the PPA](https://github.com/neovim/neovim/wiki/Installing-Neovim#ubuntu) works smoothly. No issues. This is hands down the best way to run Neovim on Windows.
- - -
Now I want [Homebrew](http://linuxbrew.sh/), but we're going to have to settle with a lot of broken packages at this point.

```
james@JAMES-PC ~> sudo apt-get install build-essential curl git python-setuptools ruby
```
```
james@JAMES-PC ~> git clone https://github.com/Linuxbrew/brew.git ~/.linuxbrew
```
__Don't run the `brew` command yet!__ First, edit your `~/.config/fish/fish.config` file

```
set -x PATH $HOME/.linuxbrew/bin $PATH
set -x MANPATH $HOME/.linuxbrew/share/man $MANPATH
set -x INFOPATH $HOME/.linuxbrew/share/info $INFOPATH
set -x HOMEBREW_BUILD_FROM_SOURCE true
```

Setting `HOMEBREW_BUILD_FROM_SOURCE` is [pretty important](https://github.com/Microsoft/BashOnWindows/issues/471#issuecomment-224405502)! Otherwise you're going to get [issues involving `Exec format error`](https://github.com/Microsoft/BashOnWindows/issues/743). On the downside, compiling everything takes forever. On the upside, brew will actually function! Kind of. Installing little tools like `ag` is good. Installing Neovim or fish from here is a no-go.

If you need to install anything that requires openssl, you'll have to precompile openssl with a flag to avoid [this bug](https://github.com/Linuxbrew/homebrew-core/issues/761).
```
james@JAMES-PC ~> brew install openssl --without-test
```
Don't worry. I'm sure those failing tests we're skipping aren't ominous at all!

![We're like dogfort knox](/images/2016/1bs7wk.jpg)
- - -
I'm going to try to unify my config files across OS X, Fedora, and Windows because I am a madman. To detect Windows, I'm using the command `cat /proc/sys/kernel/osrelease | grep Microsoft`
- - -
Next stop, tmux. It's preinstalled! An old version that doesn't support true color mode. We need at least version 2.2 for pretty colors. How to you feel about [PPAs from random people](https://gist.github.com/P7h/91e14096374075f5316e/ffef0c44907f3f247aab4d0888d116e85eb5c072)? Done!
- - -

Whew! There we are. A whole evening of work for something you could have set up in a tenth of the time using Ubuntu in a VM. But this is way more convenient that having to fire up VirtualBox, don't you agree?

A parting screenshot of using this setup to edit some ancient code for posterity:
![Editing lyrical code](/images/2016/Screenshot-2016-10-05-01.24.28.png)

---
author: "James"
categories: ["humor", "fiction"]
date: 2013-11-15T06:39:16Z
description: ""
draft: false
slug: "time-team-paradox-x-double-hitler-excerpt"
aliases:
  - /time-team-paradox-x-double-hitler-excerpt
tags: ["humor", "fiction"]
title: "Time Team Paradox X: Double Hitler (excerpt)"

---

<blockquote class="twitter-tweet"><p>Alas, the plot of my future classic &quot;Time Team Paradox X: Double Hitler&quot; will have to wait for a future <a href="https://twitter.com/search?q=%23NaNoWriMo&amp;src=hash">#NaNoWriMo</a>.</p>&mdash; James (@JamesGecko) <a href="https://twitter.com/JamesGecko/statuses/400405006605881344">November 12, 2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Hitler Prime stroked his mustache, discretely removing evidence of a recent Egg McMuffin.

"You see," he explained to the temporal assassins, "time travel via your machine in the strictest sense is clearly impossible. But parallel worlds are possible.  You did not actually travel to 1930 in your own world, you traveled to a parallel world in which 1930 happened to align to your 2041."

"But, we killed you!" stammered Biff.

"I'm afraid not," continued Hitler Prime. "You killed an alternate version of me who lived in that world. Your Hitler's actions are irreversible, your mission was a failure for your world before you started."

Victoria cleared her throat. "Wait a moment. You aren't planning genocide now, are you?"

"My dear lady, we are all products of our times. He was an artist and a dictator. I study physics and race velocipedes. Does it sound like I have designs on Poland?"

"I don't trust him," whispered Biff. "He shouldn't even be in 2041." Then, raising his voice, "You seem to know a lot about your alter ego. Significantly more than we told you, Adolphtimus Prime."

Hitler's smile faded. "You doubt me? They doubted me, too. They all doubted me!" He was yelling now. "But I quashed their doubt with SCIENCE!" He yanked a switch on the wall, and a platform rose into view. Upon it was clearly visible a hideous rip in the fabric of time and space.

"Had enough of your alternate worlds? How would you like some real time travel?!" He lunged towards Biff menacingly.


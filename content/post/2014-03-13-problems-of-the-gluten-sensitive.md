---
author: "James"
date: 2014-03-13T02:58:35Z
description: ""
draft: true
slug: "problems-of-the-gluten-sensitive"
aliases:
  - /problems-of-the-gluten-sensitive
title: "Problems of the Gluten Sensitive"

---

There are two primary problems that gluten sensitive people need solved.

Finding eatable food is challenging. Information is scattered all over the place online and in meatspace. Sometimes food has a GF label on it. There's no guarantee for most restaurants. I'd love to see a comprehensive database or wiki or something. Maybe something with a StackExchange-like reputation system so that trolls can't introduce false data that puts people in misery.

Getting glutened is worse. Sometimes a manufacturer changes their supplier for an ingredient, and trace amounts of gluten are suddenly introduced into a previously reliably-edible product.

Different restaurants in the same chain can prepare food differently; in some Subways there's a dedicated salad bowl and chopper; in other Subways they use a bread knife to chop up tomatoes and pick dropped bits off counter tops bread has recently touched.

There are some test kits available to detect this stuff, but they're expensive and inconvenient enough that I can't justify their use at my level of reaction.

My ideal solution would be a hand-held point-and-shoot laser scanner type thing (as found in every sci-fi medical bay!), but I'd settle for a subscription to a gluten free database. Why doesn't this appear to exist?


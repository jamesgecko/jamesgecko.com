---
author: "James"
date: 2020-05-17
description: ""
slug: "covid-19-mob-manipulation"
tags: ["politics", "covid-19"]
title: "COVID-19 Mob Manipulation"
---

There is mounting evidence that someone is using social media to manipulate the public. The concept isn't new, especially to state actors. But multiple sources report that bots are posting a concerning amount of misinformation about COVID-19.

> Inauthentic accounts are amplifying disinformation and inaccurate statistics and sharing false information as a reason to reopen the country," Bouzy says. "Many of these accounts are also spreading bizarre conspiracy theories..."  
> [Business Insider, 2020-04-24](https://www.businessinsider.com/trolls-bots-flooding-social-media-with-anti-quarantine-disinformation-2020-4)

> [NortonLifeLock Research Group] says it found the percentage of bot-originated tweets was as high as 20% when viewing trending topics like “#covid19”.  
> [VentureBeat, 2020-05-14](https://venturebeat.com/2020/05/14/nortonlifelocks-botsight-tool-uses-ai-to-spot-fake-twitter-accounts/)

Over the past few months there have been several seemingly grassroots "Reopen" protests in various states. As they unfolded, we learned that many of the domains used to organize the protests were [registered in bulk by a few individuals](https://krebsonsecurity.com/2020/04/whos-behind-the-reopen-domain-surge/). The protests were not grassroots efforts but [astroturfing](https://en.wikipedia.org/wiki/Astroturfing).

What does it matter if the protests are being organized by a single entity? If real people participated, doesn't that indicate a level of authentic outrage? The problem is that people weren't upset enough to protest before someone started pushing them. Further, the information being pushed isn't accurate. Propaganda doesn't run on truth. It's easy to radicalize people who are already unhappy with the current situation by lying to them.

This has happened before, back in 2014. You might remember it as GamerGate. A [relatively small](https://arstechnica.com/gaming/2014/09/new-chat-logs-show-how-4chan-users-pushed-gamergate-into-the-national-spotlight/) number of 4channers created hundreds of Twitter sockpuppets (fake accounts) to create the illusion of wide outrage over an incident that never happened. They used these sockpuppets to spread misinformation widely; anyone casually searching for information about what was happening at the time was likely as not to land on an explanation written by "gators." What was this ill-gotten influence used for? Harassing women in the video game industry. For years. The whole sordid tale is [well documented](https://rationalwiki.org/wiki/Gamergate), although this is a case where the less you know, the happier you are. 

So here we are again. This time the stakes are life and death at an unbelievable scale. Trump has stated that he is a wartime president. He is correct in that the US is under attack. Unfortunately, he has spent the past four years attempting to discredit the mainstream media. They might have otherwise provided our best defense against a foreign propaganda campaign, but now a good chunk of the US population trusts the news less than ever before. Additionally, they've occasionally fallen for the same conspiracy theories they should be protecting the population from.

So it falls to us. As a Christian, I believe that an invisible war has raged around us since the beginning of the world. This is merely a battlefield that is more visible than usual. In a war of information our weapons are truth and love.

It's important to know correct things. We need to check claims. We need to make sure that people who sound good are actually experts. We need to make sure that people who were once experts a few decades ago haven't [fallen off the deep end](https://science.thewire.in/the-sciences/luc-montagnier-coronavirus-wuhan-lab-pseudoscience/).

It's important that we not propagate false information. Any college graduate knows about evaluating the reliability of sources. It can be a bit of a judgement call sometimes, but if a site making an extraordinary claim wouldn't be credible enough to cite in a paper, maybe it's not the best thing to signal boost.

So we hesitate to spread conspiracy theories. We try to be as correct as we can be. But all that doesn't mean anything if we don't do this in love.

> If I have the gift of prophecy and can fathom all mysteries and all knowledge, and if I have a faith that can move mountains, but do not have love, I am nothing.  
> [1 Corinthians 13:2 (NIV)](https://www.biblegateway.com/passage/?search=1%20Corinthians%2013&version=NIV)

Just being right doesn't count for anything if we don't care about the people we're trying to convince.

> Love is patient, love is kind. It does not envy, it does not boast, it is not proud. It does not dishonor others, it is not self-seeking, it is not easily angered, it keeps no record of wrongs. Love does not delight in evil but rejoices with the truth. It always protects, always trusts, always hopes, always perseveres.  
> [1 Corinthians 13:4-7 (NIV)](https://www.biblegateway.com/passage/?search=1%20Corinthians%2013&version=NIV)

Online discussions would be so much more civil if this was our standard. I'm definitely not always thinking about honoring the other party when replying to comments exhorting me to, "wake up, sheeple." But I doubt I've ever changed an opinion by screaming facts into the internet. If they change their mind at all, it's only after a civil conversation. I may have to come to understand their point of view. (The horror!)

My favorite example of the effectiveness of love is [Daryl Davis](https://www.npr.org/2017/08/20/544861933/how-one-man-convinced-200-ku-klux-klan-members-to-give-up-their-robes), the African American guy who has gone around befriending members of the KKK for the past several decades. After they get to know him, they realize their prejudices are silly and they leave the klan. He credits their departures to his Christian faith.

It's not about me being right all the time. Sometimes I'm not. Jesus is the source of both love and truth. If I represent him, I've done all I can do.

---
author: "James"
date: 2016-05-21T04:35:40Z
draft: false
cover: "/images/2016/win10upgrade.jpg"
slug: "forethought-preparation-and-victim-blaming"
aliases:
  - /forethought-preparation-and-victim-blaming
tags: ["windows"]
title: "Forethought, Preparation, and Victim Blaming"

---

For the past year, Windows 7 and 8 have been demanding users to upgrade to Windows 10. For the past year, conservative users with a deep, well-paced mistrust of technology have been ignoring this request. Microsoft has had enough. It's time to [forcibly move things forward](http://www.cnet.com/news/microsoft-windows-10-upgrades-get-more-sneaky-pushy/). It's time to make computers great again.

A close friend was noticeably perturbed when their Windows 7 PC was suddenly running Windows 10. As a terrible friend, I was unsympathetic.

Microsoft is out of line, but here's the thing. My friend wasn't the nice old lady next door with a dozen browser toolbars. He writes code for a living. As a developer, you don't have to take crap from computers anymore. If something bothers you, you can generally change it. If you're skeptical about what the future holds, you take hold of your technological destiny and you point it elsewhere.

It's prudent to set aside money for an emergency. If your boss is abusive, it's wise to get out and work for someone else. Regular maintenance prevents your car from breaking down. When Windows asks you to upgrade a million times, you use Google, [disable the nag dialog, and block the upgrade](http://blog.ultimateoutsider.com/2015/08/using-gwx-stopper-to-permanently-remove.html).

If you can't be bothered to lock up your downtown apartment, nice men wearing ski masks will eventually borrow the TV. When your computer is suddenly running Windows 10 and [your last system backup was never](/-epic-fail/), you've only yourself to blame.

<blockquote class="twitter-tweet" data-lang="en"><p lang="ja" dir="ltr">Windows7/8.1ユーザーの現況を端的に表してみました <a href="https://t.co/z2NzW5om5P">pic.twitter.com/z2NzW5om5P</a></p>&mdash; 松乃雪 (@pso2tmg) <a href="https://twitter.com/pso2tmg/status/732901843290853377">May 18, 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

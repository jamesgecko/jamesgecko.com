---
title: "Portable Technology"
date: "2020-03-30"
slug: "portable-tech"
tags: ["gadgets"]
draft: true
---

In college, my first roommate was my worst. He'd spend his nights watching anime, the 30 inch behemoth CRT illuminating the room like a small sun. His days were for sleeping. I hated creeping around during normal hours. It was hard to sleep and it was hard to study. One day someone asked me why I didn't just take my laptop and books down to the student center and study there? It was a revelation.

It was also _heavy_. My textbooks weighed approximatey as much as the trees from whence they'd been huewed. My laptop was a good five pounds as well. Could 

The iPhone was present, but it only ran on AT&T. A friend had a WebOS phone. Android was an unmitigated disaster, shipping on a flagship device with a trackball and slide-out keyboard. A single classmate had an Nokia N9, which ran MeeGo Linux. He'd pop it up on a tiny stand on his desk and attach approximately three thousand wires to it, plugging into a full sized keyboard, power, and ethernet.

The Kindle had a full keyboard, no touch screen, and no backlight, but was otherwise basically identical to the modern day.

We've come a ways, but the N9 approach was probably 

. Problem solved.

Really, most consumer technology should probably be dockable.


In Andy Weir's _Artimis_, the moon version of cell phones, gizmos, have 

# Where we are

There are some neccessary tradeoffs.
- Low power
- Small screen size

## Terrible input devices

I'm not quite sure why this is mandatory, but it apparently is.

- Smartphone touchscreens are perfectly servicable until you need to do something that requires fine percision, like positioning a cursor in a text editor, and then they're a nightmare.
- The Switch JoyCons have some of the worst trigger buttons known to man.
- The Surface Go type cover is as good as a ten inch keyboard could possibly be, but it's just slightly too small and flexible.
- The less said about Apple's nightmarish butterfly switches the better. They make my fingers go numb in about thirty minutes of use. It's like banging your fingers on a solid block of alumnium.

# Where we should be

## Everything should dock

## Everything should interoperate

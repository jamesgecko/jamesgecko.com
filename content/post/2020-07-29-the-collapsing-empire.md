---
author: "James"
date: 2020-07-29
description: ""
slug: "software-of-the-collapsing-empire"
tags: ["software", "dweb"]
title: "Software of the Collapsing Empire"
---

Sci-fi is a fun genre because it lets you explore real world ideas without the baggage of real world problems. The novel [The Collapsing Empire](https://whatever.scalzi.com/about/books-by-john-scalzi/) explores what would happen if all the wormholes holding a space-age interplanetary empire together started to collapse. Let's roll with that a bit closer to home. What would happen if the wormholes connecting the USA planet to the rest of the country planets collapsed? We're a software powerhouse. What would the world do if all our major tech companies just up and vanished?

The first thing that comes to mind is that closed source software developed in the US stops getting updates. That's a real problem for security! Especially consumer operating systems. Either another entity somehow gets a copy of the code and starts releasing patches for Windows, or lots of computers are in real danger of being assimilated into botnets. Linux isn't the best desktop experience, but maybe it's all we've got! Linux and some BSDs. Maybe ReactOS? Fuchsia? Alas, Haiku doesn't seem to have decent hardware support. Or maybe the Windows or macOS source leaks and they're carried on as open source projects.

The next thing that we run into is that a lot of important online infrastructure is in the US. Losing AWS, Google Cloud, and Azure takes out tons of important websites. Maybe I'm wrong and the loss of major US hosting companies would be mitigated by regional hosting and local maintenance teams. (`Amazon.com` is down, but `Amazon.ca` is here to stay!) But I suspect websites would run into the same issue as closed source operating systems. When the main engineering team is unavailable, there's hardly anyone left to keep the bit rot at bay.

But imagine that they're just gone. How do we cope with that? I think small distributed applications are probably the way to go. Either federation, like email, where losing one provider doesn't take out the service for everyone. Or peer-to-peer applications, like BitTorrent, where every user hosts content.

On the web browser side, Beaker Browser (Hyperdrive) or IPFS are excellent technologies to bet on. Anyone can mirror a website for anyone else.

Matrix is another tool to watch. An encrypted, open source messaging protocol. Right now it's only federated, but they're working on a distributed version as well.

Dropbox, Google Drive, and OneDrive are gone. Resilio Sync would technically be around, but It's closed source by a US development company. SyncThing is probably a better option.

P2P social networking is complicated. It's not that there aren't any options, but few of them offer private posts as anything other than a coincidence of network topology. [Secure Scuttlebutt](https://scuttlebutt.nz/) and the polished [Manyverse](https://www.manyver.se/) are about the best we've got. Mastodon and other ActivityPub software probably copes well in this scenario, too. It's federated, not peer to peer, but if half the known servers dropped out, the rest of the "fediverse" would keep chugging along.

It seems critical that protocols and formats used by our new world of applications would be small-ish and open, with multiple implementations. We're distributing developers as well as hosts for additional resiliency.

It should be noted that most of these are desktop-only for now. That brings us to another issue. We're sort of in a weird halfway point between x86 and ARM tech. The only way to write an application that anyone can run on any device is still a web app. It'd be great if we could pair [web bundles](https://web.dev/web-bundles/) with our distributed browsers and swap web apps on USB sticks, but we're not quite there yet. If a huge chunk of the browser teams at Google, Mozilla, Apple, and Microsoft are gone, who is pushing web standards forward? Or even maintaining browsers? Chrome is a beast; a full time job for a large team. My guess is that standards suddenly slow way down and everyone lives with present day browser technology for a while.

This thought exercise kinda puts me in the mind of the [Small Technology manifesto](https://small-tech.org/about#small-technology). I don't complete agree with all of it (commercial software is fine within reason), but the broad strokes are in the right places.

This exercise also makes the hardcore free software crowd look great. I've swung from "free as in freedom" to pragmatism due to the flagrantly terrible UX and QA on display in a lot of projects. But I dunno. Free software kinda lost on the desktop and mobile. It barely ran in online services in almost any way that mattered to users. It's all a bit depressing, especially as we careen towards a point in history where this thought exercise may become less theoretical.

The projects I noted above are promising, but any developer knows that putting faith in software is foolish. It will either let you down in the future, or it has already let you down and you just haven't realized yet.

> Now I know that the LORD saves his anointed;  
>   he will answer him from his holy heaven  
>   with the saving might of his right hand.  
> Some trust in chariots and some in horses,  
>   but we trust in the name of the LORD our God.  
> They collapse and fall,  
>   but we rise and stand upright.  
>
> -- Psalms 22:6-8 (ESV)

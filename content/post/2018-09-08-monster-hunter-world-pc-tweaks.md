---
author: "James"
categories: ["games"]
date: 2018-09-08T21:10:14Z
description: ""
draft: false
slug: "monster-hunter-world-pc-tweaks"
aliases:
  - /monster-hunter-world-pc-tweaks
tags: ["games"]
title: "Monster Hunter World PC tweaks"

---

The PC port of Monster Hunter World is a bit of a hot mess. I've collected a number of fixes, now together for the first time. This list is current as of 2018-9-8.

## Windows video codecs
If you don't use _Windows 10 N_ or live in Europe, skip this section.
If you're still here, there's a chance that your version of Windows is missing some video codecs. Without these codecs, your game will auto-save and crash right before final boss.

- https://www.microsoft.com/en-us/software-download/mediafeaturepack
- https://www.microsoft.com/en-us/p/web-media-extensions/9n5tdp8vcmhs?activetab=pivot:overviewtab

## Save backups
The game does not use rolling save files. What this means is that if the game crashes while it is saving, your save file will be corrupted. Steam Cloud won't protect your from this; it'll immediately back up the corrupted save file.

[/u/Chrushev wrote a script](https://www.reddit.com/r/MonsterHunter/comments/973xjq/i_wrote_a_script_to_automatically_backup_monster/) that you can use to periodicly back up your save.

## Optimal in-game graphical settings

- https://www.eurogamer.net/articles/digitalfoundry-2018-what-does-it-takes-to-run-monster-hunter-world-pc-at-1080p60
TL;DR: 
- The "optimized" console-quality settings (13:23 in the video)
  - Ambient occlusion: Medium
  - Shadow quality: High
  - LOD Bias: High
  - Max LOD Level: No limit
  - Sh Diffuse Resolution: Low
  - Volumetrics: Variable
  - HDR: 32-bit
  - SSR, Water reflections, vegitation sway, SSS: On
- Turning volumetrics off gives a huge performance boost, at the cost of making rainy and foggy environments look bad.

## Driver profile

NVidia screwed up their driver profile for the game. You can get a ~20% performance improvement in Rotten Vale by disabling a profile option via NVidia Inspector.

> This specific setting in the Monster Hunter Profile located under the Unknown Section at the last section. It is titled "0x0094C537 (2profiles)" with values of "0x00803007 (Monster Hunter World)"

Set it to `0x00000000`

- NVidia Inspector: [German language website](https://orbmu2k.de/tools/nvidia-inspector-tool) | [latest release](https://github.com/Orbmu2k/nvidiaProfileInspector/releases)
- [Original thread](https://steamcommunity.com/app/582010/discussions/0/1737715419887955101/)

## Mods

Note: use these at your own risk. Capcom hasn't banned anyone so far, but this is a multiplayer game.

### [Low texture resolution workaround](https://www.nexusmods.com/monsterhunterworld/mods/46)

Fixes how some textures in the game are always super low resolution, no matter what your graphical settings are.


### [Special K](https://steamcommunity.com/app/582010/discussions/3/1745594817439431537/)

- Eliminate the abusive SwitchToThread (…) CPU hog
- Restrict job threads to a manageable number
- Fixes frame pacing issues caused by poor XInput hot-plug design
  - Eliminates regular interval hiccups if no gamepad is connected
  - Eliminates stutter during gamepad hot-plug event
- HUDless screenshots
- Texture/button mods
- Pre-HUD ReShade
- Fixes for Fullscreen Exclusive problems


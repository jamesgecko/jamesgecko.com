---
author: "James"
categories: ["gluten", "life"]
date: 2014-01-22T07:03:49Z
description: ""
draft: true
slug: "two-months-with-gluten-intolerance"
aliases:
  - /two-months-with-gluten-intolerance
tags: ["gluten", "life"]
title: "Three Months with Gluten Sensitivity"

---

Gluten sensitivity is a terrible disease which causes one to become a hipster. Those affected suddenly are extremely picky about what they eat and where their food came from.

On December 9th, 2013, I discovered that I had joined their ranks. No testing; just dietary experimentation. My sister also has issues with gluten (and a formal diagnosis), so I doubt this is a fluke. Nevertheless, I was initially hesitant to accept my self-diagnosis for a few reasons. First is that the brain is kind of crazy. [It can manufacture reality, to an extent](http://www.wired.com/medtech/drugs/magazine/17-09/ff_placebo_effect?currentPage=all). After over a month of non-trivial stomach pain, I was afraid that my brain might accept a gluten free diet as the cure and adjust my immune system accordingly, even if the root cause was initially something else. The placebo effect works even when the patient knows they're on a placebo.

Placebo or not, it worked. Not immediately; first I spent a week not eating any of the food I normally eat and being generally unpleasant to be around. I've read of people who say they become more irritable when they eat gluten, but I missed bread so much. I just got mean when I quit. Even a heightened level of self analysis provided little emotional control. It was a foreign experience.

The only people who care almost as much about the technical details of their food more than people with allergies or sensitivities are the earthy crunchy folk. I find myself locating recipes on gluten-free websites which spout off all sorts of non-scientific nonsense. It is an uneasy truce.

It's surprisingly difficult to explain gluten to uninformed employees in restaurants. It is possibly inaccurate (but a useful shortcut) to just say that I am allergic to gluten. This still isn't enough, because some people don't know what it is, other people think that gluten == wheat, and yet others believe that every grain contains gluten. Once the basic facts of the matter have been conveyed, the waiter will go off to deliver food which may or may not have come in contact with enough gluten to set me off and give me stomach pain for half a week.

They can't help it. Gluten is in like half the stuff at the grocery store. I have become suspicious of strange food. Simple foods, you can mostly trust. Anything syrupy or thick or springy or delicious is suspicious. Change is scary. Some brands of cornmeal are processed in an environment near wheat; buying a different brand can result in an unpleasant surprise. Chipotle is now my favorite fast food restaurant, because I can eat almost everything on the menu, and they have a big detailed chart on their website.


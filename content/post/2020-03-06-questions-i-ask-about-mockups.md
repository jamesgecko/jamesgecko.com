+++
title = "Questions I Ask About Mockups"
date = "2020-03-01"
slug = "questions-i-ask-about-mockups"
aliases = ["/questions-i-ask-about-mockups"]
tags = ["dev"]
+++

There are many essential skills for the software developer who hopes to advance in the world. Let's talk about two of them.

1. The ability to ask good questions.
2. The ability to estimate how complex something will be off the cuff.

These skills combine when reviewing the specification for a brand new feature. A small group of engineers sit around a mockup as the product manager or designer explains what the feature does. As you gaze at the mockup, there's a tickle of doubt at the back of your mind. There's something here that could be a lot of trouble if we don't ask some good questions right away, you're sure of it. But what could it be?

Here's a list of questions I've started asking myself in these situations.

## How can this fail?

Could this feature require a heavy query for large customers? Is it possible that the server could time out? Or returns a 500 error? Is there form validation? What is the copy for form fields that fail? What feedback to we display when these errors happen; what do they look like?

## Which parts of this need to load over the network?

It's common to see wizards where the user inputs data on step two, and step three shows information obtained by sending that data to the server. Or even a dropdown menu populated by a form input above it.

Related to this, what does it look like when these things are loading? Does the entire form disappear? Do we just disable everything and show a spinner on the submit button?

## What roles can see what?

Administrators can do everything, obviously. But we also have read-only roles and roles that can change some settings (but not _important_ settings), and so on. It's awkward to realize halfway through development that you don't know what things are supposed to be visible for which roles.

## If there's support link, what URL does it link to?

Some of our features are hairy enough that we'll connect people directly to support. Other features are often adequately explained with a support document. Either way is fine, we just need to have the information before we ship (and preferably before we start writing code).

## Is there anything that isn't in our UI-Kit?

Our simple collection of reusable GUI widgets has grown in scope; once a few buttons and dropdowns, it is now hundreds of battle tested UI elements. It's often possible to toss a bunch of existing widgets on a page and link them together for a serviceable front end. If there's a generic-ish widget that seems like it might be reused in a future design, we sometimes decide to split out a task to add the widget to the UI-Kit for future use.

## Is there anything we're assuming can be reused from something else?

If a mockup has a data table that seems similar to a table on a different page, we should check to see if the other table is reusable. Could it easily be refactored to be reusable? Are we assuming that the data table is reusable, but really it would be an unreasonable amount of effort to reuse it from the other feature?

## Does this widget work the way the design shows?

Very occasionally a design will feature an existing UI widget being used in a novel way that the code doesn't support. Is this intentional? We don't want to shut down what could be a reasonable change to the widget, but we do want to identify what could be a time-intensive task so that it doesn't come as a surprise a week after we've started the project. Sometimes we change the widget. Other times the differing behavior was unintended and the designer is able to revise the mockup to better fit with the existing behavior.

## Can anything expire?

Time, our tireless foe. Does this use an authentication token that can expire? An invitation that is only valid for two weeks? What if the user's session expires and they're logged out? What happens after these expirations? Also see, how can this fail?

## Are there accessibility challenges?

This is extremely important but can be hard to answer. "Does this have WCAG AA contrast?" is low hanging fruit. Asking, "what does this new widget look like when it has the keyboard focus outline?" can also be valuable. In the back of my head I also wonder how a screen reader might read the information in the design.

## How does this work with white label users?

We offer a rebranded version of our product for resale. It's important that our company name is filed off anything that isn't visible to an admin. But what happens if this feature requires the user to authenticate with our product via a third party service? We can't really hide our brand on the third party. Or what if this image asset uses our brand colors? Constant vigilance!

# TL;DR

- How can this fail?
  - How should errors be displayed?
- Which parts need to load over the network?
  - What does it look like when those things are loading?
- What roles can see what?
- If there's a support link, what URL does it go to?
- Is there anything that isn't in our UI kit?
- Does this widget work the way the design shows?
- Is there anything we're assuming can be reused from something else?
- Can anything expire? (Sessions, invitations, authentications).
  - What happens after the expiration?
- Accessibility
  - What does keyboard outline focus look like?
- How does this work with white label users?

This _is_ a lot to remember. I still miss things now and then. But [checklists are good!](https://www.hsph.harvard.edu/news/magazine/fall08checklist/). I'll be pulling up my cheat sheet during meetings. It won't save lives, but with grace maybe it'll save us some time.

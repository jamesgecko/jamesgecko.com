---
author: "James"
date: 2013-10-17T07:05:51Z
description: ""
draft: true
slug: "lazier-than-sed"
aliases:
  - /lazier-than-sed
title: "Lazier than sed"

---

Perhaps you need to automatically edit text files, and you know vim, but you are too lazy to learn sed. What if you could record a vim macro on a sample file, then save it to a string and execute it on arbitrary files on the command line? But of course.

- Open up your text file.
- Record the macro to the q register via `qq`.
- Now make a new buffer and paste the keystroke string. `”pq`
- Save that sucker.
- Execute it with a command like `vim -c $(cat macro.vim) myfile`
- It doesn’t work for some reason.


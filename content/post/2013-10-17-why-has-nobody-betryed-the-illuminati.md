---
author: "James"
date: 2013-10-17T07:07:35Z
description: ""
draft: true
slug: "why-has-nobody-betryed-the-illuminati"
aliases:
  - /why-has-nobody-betryed-the-illuminati
title: "Why has nobody betryed the Illuminati?"

---

The Illuminati. A secret organization which controls everything. While they exist, no government can be truly free. That’s what they say, anyway. It sounds like a huge organization, though. Far smaller conspiracies have been definitively exposed. How many people are in on it?

As given to me, here are things in the US that the Illuminati control:

- All of congress
- The President
- Virtually every government agency that matters
- Every other government in every other country
- Celebrities

I don’t trust conspiracy theorists, so we’ll ignore their “research” and try to step through this with some back-of-the-envelope approximations.

We’ll start with the US. There are 535 senators and representatives in congress. We’ll need dirt on the majority of them to keep them under control. Say we have one conspiracy agent per three lawmakers and a manager per every ten agents. 178 agents, 17 managers, and a super-manager over the managers.

But wait! That’s just the most recent batch of lawmakers. There have been 113 delegations total. Here’s where things get complicated. A lot of lawmakers repeat in various delegations. Some lawmakers were non-voting. There’s been _a few thousand_ unique lawmakers. Plus their aides and families. Plus their handlers, as estimated above. Nobody talked. All of them were controlled by the Illuminati.

Or were they? At this very moment, we are in the middle of the 18th government shutdown. Nobody is benefitting from it, and the Tea Party is allegedly responsible. A house divided against itself cannot stand; the conspiracy can't control both sides. So which side is compromised?

One option is that the Tea Partiers are under Illuminati control. In this case,


---
author: "James"
categories: ["code", "atom"]
date: 2016-07-06T21:19:03Z
description: ""
draft: false
slug: "disable-code-folding-in-atom"
aliases:
  - /disable-code-folding-in-atom
tags: ["code", "atom"]
title: "Disable Code Folding in Atom"


---

The developers of the Atom editor [decided not to make code folding easily configurable](https://github.com/atom/atom/pull/5844#issuecomment-165865157). 

To turn it off, add this to `init.coffee`:
```coffee
# Disable code folding
{TextEditor} = require('atom')
TextEditor.prototype.isFoldableAtBufferRow = -> false
```

To [disable the large, empty margin](https://discuss.atom.io/t/how-do-you-turn-off-folding/12371/14), edit `styles.less`:
```less
// Disable code folding margin
atom-text-editor::shadow .gutter .line-number .icon-right {
  display: none;
}
```


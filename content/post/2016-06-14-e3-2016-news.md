---
author: "James"
categories: ["games"]
date: 2016-06-14T01:50:32Z
description: ""
draft: false
slug: "e3-2016-news"
aliases:
  - /e3-2016-news
tags: ["games"]
title: "E3 2016 News"

---

All the reporting everywhere is kinda boring. It turns out that this is because E3 is boring this year. I have taken the liberty of summarizing the highlights.

# Hardware
- Microsoft announced two new consoles. One of them is an old console.
- Sony announced that they will announce a new console.
- Nintendo will not be announcing that they will announce a new console.
- Several companies want to sell very expensive hats that hold cell phones in front of your face.

# Games
## ReCore

It's a AAA 3D platformer and it has Mega Man and Metroid Prime people making it. Also, it has a grappling hook.

<iframe width="560" height="315" src="https://www.youtube.com/embed/WnKT9ZCGgBE" frameborder="0" allowfullscreen></iframe>

Windows 10 is getting more Xbox One exclusives with cross-buy and cross-saves. This is one of them. Maybe some of them will have cross-platform play, but I'm not holding my breath for that when Halo 6 inevitably rolls around.

## Beyond Good and Evil 2

[It's not cancelled!](http://www.polygon.com/e3/2016/6/13/11926826/beyond-good-evil-2-is-still-coming-ubisoft-ceo-confirms) Game of the show!

## Tyranny

In a shocking twist, Obsidian announced another RPG about making choices.

<iframe width="560" height="315" src="https://www.youtube.com/embed/uR0K3yEtmo8" frameborder="0" allowfullscreen></iframe>

## Mass Effect Andromeda

Mass Effect? Mass Effect. Mass Effect Mass Effect Mass Effect Mass Effect. Mass Effect!

<iframe width="560" height="315" src="https://www.youtube.com/embed/y2vgHOXeps0" frameborder="0" allowfullscreen></iframe>

## Scalebound
It's **Platinum**, and they're making a Monster Hunter game. But the trailer looks awful. But it's Platinum. I want to believe!

<iframe width="560" height="315" src="https://www.youtube.com/embed/D-ZVMU_KA-M" frameborder="0" allowfullscreen></iframe>

### Overwatch
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">*Blizzard gets on stage at E3*<br><br>&quot;We already released Overwatch&quot;<br><br>*INTENSE SCREAMING*<br>*PEOPLE FAINT FROM HAPPINESS*<br>*RYUU GA WAGA TEKI WO KU*</p>&mdash; ✩Genius Oniony✩ (@LolicOnion) <a href="https://twitter.com/LolicOnion/status/742407516831768576">June 13, 2016</a></blockquote>

<iframe width="560" height="315" src="https://www.youtube.com/embed/0zo3F7-G9ss" frameborder="0" allowfullscreen></iframe>

## Apple
Apple's E3 conference surprised a lot of people by [not having anything to do with video games at all](http://www.theverge.com/2016/6/13/11921068/apple-ios-10-announced-new-iphone-features-wwdc-2016). Very strange from a company with such a large stake in mobile gaming.


## Best trailer for the worst game
<iframe width="560" height="315" src="https://www.youtube.com/embed/BzxhVkeWgPY" frameborder="0" allowfullscreen></iframe>


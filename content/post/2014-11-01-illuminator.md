---
author: "James"
categories: ["games", "game design"]
date: 2014-11-01T06:00:37Z
description: ""
draft: false
slug: "illuminator"
aliases:
  - /illuminator
tags: ["games", "game design"]
title: "Illuminator"

---

Some people like scary games. I'm not really an _Amnesia: Dark Descent_ kind of guy. My favorite horror game is this little bugger, _Illuminator_, which I pull up every Halloween. It's from a project on the TIGSource forums called [Action 52 Owns](http://www.superfundungeonrun.com/action52/).

It looks like this:
![Illuminator screenshot](/images/2014/Nov/Screenshot-2014-11-01-01-34-05.png)
Let's talk about why it's great. As you can see, it's dark and you have a flashlight. What you don't see is that the house is full of monsters.

The main mechanic is that vision is a limited resource. Your battery drains when the flashlight is on and recharges when it's off. When sufficiently charged, turning the flashlight on shoots a huge blast of light that burn up monsters (and drains the battery). _You need to turn off your flashlight to fight monsters._

The secondary mechanic builds on that. You can't have you flashlight on all the time, but you can set up limited light sources in strategic locations to reveal monsters. But there's never quite enough light to go around. It's startling when a monster gets past your dimly lit defenses and attacks you.

It's a game about gaps in information. The scariest thing is what lurks in the gaps.

[Download for Windows](http://www.l-ames.com/logan/illuminator.zip)

> Since there's no readme, and it's not super intuitive:
> 
> - `X` is the pick-stuff-up, plug-stuff-in, open-doors key.
> - `C` turns the flashlight on and off
> - If nothing is happening, you need to blast the rift thingie.

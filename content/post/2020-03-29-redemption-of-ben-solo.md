+++
title = "The Redemption of Ben Solo"
date = "2020-03-29"
slug = "the-redemption-of-ben-solo"
aliases = ["the-redemption-of-ben-solo"]
tags = ["star-wars", "faith"]
+++

_The Rise of Skywalker_ is divisive. People have voiced numerous opinions, but the one specific complaint sticks with me. It's about how Kylo Ren is shown mercy and repents. He dies hours later, having reclaimed his true identity of Ben Solo -- yet having done little to deserve his conversion. I've heard the opinion that the movie would have been better if the moral was that some people _can't_ change!

While Ben's redemption as written isn't necessarily satisfying storytelling, I can't agree with the premise of the complaint. Fantasy is compelling because it can show truth through a different lens. My faith is extremely specific about ultimate redemption. It's not based on individual merit, and anyone can qualify. A mass murderer could be struck with conviction on their deathbed, pass into eternity, and be welcomed by my savior into the embrace of Heaven; a relationship of endless love and joy. This is somehow ultimate, cosmic justice.

The Star Wars sequel trilogy updates classic antagonists for modern times. The original trilogy was loosely inspired by World War II films, so the Empire were essentially Nazis. In Episodes 7-9, the First Order are based on Neo Nazis; still evil, but less coordinated and competent. It's a more familiar evil.

We were, as a culture, more or less okay with Anakin Skywalker being redeemed. He was responsible for genocide! Some he cut down personally, others he ordered killed with a weapon of mass destruction (RIP Alderaan). But Ben Solo was arguably responsible in some fashion for five times the number of deaths through Starkiller base alone. But once the bodies reach the billions, who's counting anymore? By any human measure, both murderers should be irredeemable.

Yet Ben Solo is different somehow; _familiar_. We don't personally know anyone who acts like Darth Vader, but we see or hear about people who behave like Kylo Ren all the time. We don't want to entertain the thought that such people could be turned; we'd like them to die. We see the sin and rightly judge that death would be justice.

But most won't admit that they're also guilty of sin. A harsh word to family member? Sin. Not to anywhere near the same _volume_ as genocide, but sin is like a burst pipe in the basement. It doesn't matter if there's an inch of water or six feet, the plumber gets called either way. By default, the posthumous justice of humanity deserves eternal death.

Escape from eternal death depends on the merit of the Savior who gives freely to all to ask.

> For the wages of sin is death, but the free gift of God is eternal life in Christ Jesus our Lord.
>
> [_Romans 6:23 (ESV)_](https://www.biblegateway.com/passage/?search=Romans+6&version=ESV#en-ESV-28073)


> For by grace you have been saved through faith. And this is not your own doing; it is the gift of God, 9 not a result of works, so that no one may boast.
>
> [_Ephesians 2:8-9 (ESV)_](https://www.biblegateway.com/passage/?search=Ephesians+2&version=ESV#en-ESV-29221)

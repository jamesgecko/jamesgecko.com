---
author: "James"
date: 2011-01-26T00:00:00Z
description: ""
draft: false
slug: "in-which-a-sibling-learns-to-play-minecraft"
aliases:
  - /in-which-a-sibling-learns-to-play-minecraft
title: "In which a sibling learns to play Minecraft"

---

I set my younger brother Matthew up playing Minecraft yesterday.

First, WASD-style controls are a mystery to him. Even after explanation, he moves around using only the W key and mouselook.

Matthew: Hey look! I found a goat.

James: What? There aren't any goats in this game...

Goat: SSSSSssss.

James: Aaaaah! That's not a goat! Run!


---
author: "James"
date: 2010-04-03T00:00:00Z
description: ""
draft: false
slug: "theme-rain"
aliases:
  - /theme-rain
title: "theme:rain"

---

<a href="/theme-rain/theme-rain.png"><img style="float: right; padding: 10px;" title="them-rain2-png" src="/theme-rain/theme-rain.png" alt="Screenshot" width="216" height="216" /></a>

theme:rain is a small game I wrote in about ten days for Dr. Tevis' computer graphics class. I believe it was my second or OpenGL application. It shows somewhat.

> For many year evil green square empire may oppressed the lowly yellow square. Now times have come. Take fight and win! Use arrow keys to defeat menace. Smash yellow invincible ship into fragile green hull! Bonus score buy frequent collision! Beware; the red box fire evil rejuvenation. Survive impossible odds in wet rain from sky. Commander... the ship is yours and win.

Probably subconsciously inspired by <a href="http://www.asahi-net.or.jp/~cs8k-cyu/index_e.html">Kenta Cho</a>, <a href="http://www.kongregate.com/games/LoFiMinds/xwung">Xwung</a>, and <a href="http://www.kongregate.com/games/TerryCavanagh_B/bullet-time">Bullet Time</a>, this is a game about balance. The goal is to use the yellow square to break as many green boxes as possible as fast as possible. The catch is that when a green box is broken, four red bullets are fired at you from each side of the screen. The faster you break boxes, the more bullets will be on the screen. The idea was to implement a bonus multiplier also, but I ran out of time. I have a vague memory of implementing it later, but I've lost any subsequent executables and also the source code.

The two colored rectangles on the main screen are buttons; click on them to exit or start the game, respectively.

The appearance of the game is right out of <a href="http://www.flickr.com/photos/tigsource/3382678778/">Cactus' IGS presentation</a>. The music, <em>DirtyBeta</em>, is a remix of a Sonic Crackers level theme by <a href="http://ocremix.org/remix/OCR00480/">Malcos</a>.

## [Download](/theme-rain/theme-rain.zip)

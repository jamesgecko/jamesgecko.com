---
author: "James"
categories: ["dev"]
date: 2019-03-03T21:17:45Z
description: ""
draft: true
slug: "making-pretty-pull-requests"
aliases:
  - "/making-pretty-pull-requests"
tags: ["dev"]
title: "Making Pretty Pull Requests"

---

The introductory text you write in your GitHub pull request is pivotal. It establishes context, describes what you implemented, and notes potential trade-offs that reviewers should pay attention to. Unfortunately, nobody ever reads the whole thing, and sometimes GitHub's documented markup isn't quite enough. Here are a few tricks to make things pop.


## Do the normal stuff first

Read [GitHub's excellent markdown guide](https://guides.github.com/features/mastering-markdown/). Headings make it easier for people to skim. Review your own PR to highlight sections of code that you'd like other people to look at.

Use good judgement when applying the rest of these techniques. My coworkers use modern browsers and don't rely on assistive technology, so I can abuse HTML markup a bit more than normal.

## Collapsible sections

<script src="https://gist.github.com/jamesgecko/0e9fdbd3439e5402bd2440bda8c89606.js"></script>

    <details><summary>CLICK ME</summary>
    <p>
    
    #### yes, even hidden code blocks!
    
    ```python
    print("hello world!")
    ```
    
    </p>
    </details>


These are also super nice if you want to include a bunch of screenshots. I'll add a **Screenshots** heading, then include a list of details tags.

<script src="https://gist.github.com/jamesgecko/4b6a7415f0d3b81abe7a27781ee3440c.js"></script>

```
### Screenshots

<details><summary>🏞 On initial load</summary>
<img src="https://jamesgecko.com/images/toot-cafe-1.png" alt="Toot.cafe screenshot">
</details>

<details><summary>🏞 With some content</summary>
<img src="https://jamesgecko.com/images/toot-cafe-1.png" alt="Toot.cafe screenshot">
</details>

<details><summary>🏞 With an error</summary>
<img src="https://jamesgecko.com/images/toot-cafe-1.png" alt="Toot.cafe screenshot">
</details>
```

## Create tables

### Lists with emojis

I use these to flag topics or sections I'd like reviewers to pay particular attention to. If I have a list of things that a PR updates, sometimes I'll use a table like this:

<script src="https://gist.github.com/jamesgecko/5faf090c4d9d5dbd89206faa71a62cc6.js"></script>

```
.|.
---|---
&nbsp;|This is a normal line.
♿|Here's an a11y issue this PR addresses.
❓|I did a specific thing in this PR. What do people think about this? Sometimes this can be a review comment instead.
```

### Images in Two-column images (good for portrait screenshots)

<script src="https://gist.github.com/jamesgecko/9e255555a313cb7c81cac2141650f22f.js"></script>

```
1 | 2
--- | ---
Light version| Dark version
![](https://jamesgecko.com/images/toot-cafe-1.png) | ![](https://jamesgecko.com/images/toot-cafe-2.png)
Another screenshot | Yet another screenshot
![](https://jamesgecko.com/images/toot-cafe-1.png) | ![](https://jamesgecko.com/images/toot-cafe-2.png)
```

## Put borders on images

Sometimes I'll have a couple screenshots with white backgrounds that I don't want to bleed into each other. I need a border or something on the image.

<script src="https://gist.github.com/jamesgecko/f4c248431c4e41026017e85794c234c0.js"></script>

### Method one: quotes 
```
> ![radio buttons](https://jamesgecko.com/images/radio-buttons.png)
```

### Method two: abuse &lt;kbd&gt;.  
You _must_ use the img tag, not the markdown img format.
```
<kbd><img src="https://jamesgecko.com/images/radio-buttons.png" /></kbd>
```
### Method three: tables
```
<table><tr><td>
    <img src="https://jamesgecko.com/images/radio-buttons.png" />
</td></tr></table>
```

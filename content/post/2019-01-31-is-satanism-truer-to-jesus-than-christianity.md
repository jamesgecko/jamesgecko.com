---
author: "James"
categories: ["cults"]
date: 2019-01-31T02:40:20Z
description: ""
draft: false
slug: "is-satanism-truer-to-jesus-than-christianity"
aliases:
  - /is-satanism-truer-to-jesus-than-christianity
tags: ["cults"]
title: "Is Satanism truer to Jesus than Christianity?"

---

Polygon [posted about a Satanism documentary](https://www.polygon.com/2019/1/30/18204077/hail-satan-documentary-review-church-of-satan-sundance-2019) today. This statement halfway through caught my eye:

> The other tenets [of Satanism] — which include “one’s body is inviolable, subject to one’s own will alone” — have been [described](https://www.salon.com/2015/11/28/the_greatest_trick_the_satanists_ever_pulled_they_may_be_truer_to_the_words_of_jesus_than_most_christians_partner/) as possibly being “truer to the words of Jesus than most Christians,”...

That’s an interesting tenet to associate with Jesus, especially since his own apostles disagreed with it. In 1 Corinthians, Paul wrote,

> Flee from sexual immorality. All other sins a person commits are outside the body, but whoever sins sexually, sins against their own body. Do you not know that your bodies are temples of the Holy Spirit, who is in you, whom you have received from God? **You are not your own;** you were bought at a price. Therefore honor God with your bodies.  
> [_1 Corinthians 6:18-20_](https://www.biblegateway.com/passage/?search=1+Corinthians+6&version=NIV#en-NIV-28486)

Emphasis mine. Paul is saying that followers of Jesus were bought by his death and resurrection. Their bodies are not subject to their will alone. They are subject to God's will as described in the Bible, at the very least in the context of abstaining from sexual activity outside of wedlock. One could object that these are Paul's words, not Jesus', but it turns out that Paul is extrapolating from Jesus' most famous sermon.

> Do not think that I have come to abolish the Law or the Prophets; I have not come to abolish them but to fulfill them.  
> [_Matthew 5:17_](https://www.biblegateway.com/passage/?search=matthew+5&version=NIV#en-NIV-23252)

The Law he referred to here includes the ten commandments. The seventh is, _Thou shalt not commit adultery._ Both Jesus and his audience are familiar with this, but he ups the ante.

> “You have heard that it was said, ‘You shall not commit adultery.’ But I tell you that anyone who looks at a woman lustfully has already committed adultery with her in his heart.  
> [_Matthew 5:27-28_](https://biblehub.com/niv/matthew/5.htm)

Jesus has expanded the explicit definition of sexual immorality. A follower of Jesus don't _just_ not own their body. Followers _also_ need to be careful at what they look at and why they look at it. It doesn't sound like Jesus thinks that the body is, "subject to one's own will alone," does it?

Jesus then uses physical mutilation as a metaphor for spiritual cleansing.

> If your right eye causes you to stumble, gouge it out and throw it away. It is better for you to lose one part of your body than for your whole body to be thrown into hell. And if your right hand causes you to stumble, cut it off and throw it away. It is better for you to lose one part of your body than for your whole body to go into hell.  
> [_Matthew 5:29-30_](https://www.biblegateway.com/passage/?search=matthew+5&version=NIV#en-NIV-23262)

An organization named after the father of lies is promoted by falsehood? _Weird!_




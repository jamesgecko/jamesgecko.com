---
author: "James"
date: 2015-03-13T05:19:04Z
description: ""
draft: true
slug: "how-3ds-drm-should-work"
aliases:
  - /how-3ds-drm-should-work
title: "How 3DS DRM should work"

---

Nintendo has a thing about making a modern account system for their consoles. They seem skittish about letting people access eShop games they bought, which in turn makes me skittish about buying eShop games from them.

I've played games on aged Atari 2600 machines. Would those games still function if I'd had to call Atari to convince them to do a system transfer?

### How system accounts should work

- When a user signs in on a 3DS they are immediately granted access to their account.
- Any other 3DSs that were authorized to that user's account are immediately deauthorized and signed out the next time they connect to the internet.
	- A downloaded eShop game on deauthorized 3DSs stays downloaded but stops working until an account that has a license for them signs in.

Boom. Problem solved. But we could go further. There's something nice and tangible about physical cartredges which makes me feel better about spending too much money on pixels on a screen. But we also have huge SD cards now, and carrying around a dozen little squares of plastic is non-ideal; if I loose a cartredge, it's gone.

### How game cartredges should work
- Every cart has a license key on it. Not printed on it, but on the internal storage of the cart.
- If you just put the cart in your 3DS, it works like a carts currently do.
- But every cart will also allow you to install it to your system's disk as a digital game. You must be logged in online to do this. Once installed, you don't need the cart in the 3DS to play anymore.
- If the game is installed on system 1 and the cart is played or installed in system 2, and system 2 is online, the DRM server would deauthorize the game on the first system until...
	- It is bought from the eShop.
    - The same cart is placed in the system.
	- A different cart of the same game is placed in the system. The user is asked if they want to use the license on this cart to reactive the installed version. It is licensed to their system upon user approval.
    
Key points in this design:

- If you have the physical cart in your system, you can always play the game.
- If you lend the cart out, your digital copy will stop working until the game is returned or replaced.
- It's a license system based around physical artifacts. The person with the cart effectively posesses both the physical and digital versions.

Drawbacks with this design:

- If the game is installed on system 1 and then the cart is placed in a second system, and the second system never goes online, the game can be played on two systems at a time.
- If the game is installed on system 1, and system one never goes online after installation, the game could be played on a second system at the same time.
- Given the appeal of online content, this seems less likely. Many games have free online DLC or multiplayer, Pokemon Shuffle is fun, and everybody loves SpotPass.


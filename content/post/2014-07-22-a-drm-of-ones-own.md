---
author: "James"
categories: ["ebooks", "drm"]
date: 2014-07-22T05:08:13Z
description: ""
draft: false
slug: "a-drm-of-ones-own"
aliases:
  - /a-drm-of-ones-own
tags: ["ebooks", "drm"]
title: "A DRM of One's Own"


---

I have all these DRM-free ebooks, and I like to loan them out. This is an action I often do with physical books. But my mechanism for doing so with ebooks is pretty crude. It's basically sending someone a copy of the book and saying "Here's a thing, delete it when you get done and let me know, honor system."

What I'd really like to do is just have a list where friends could check out a book from me and I could yank it back if they were slow to return it. We casually browse friend's physical bookshelves. Why can't this work with digital books? Ownership and convenience without incidental piracy. Personal DRM.


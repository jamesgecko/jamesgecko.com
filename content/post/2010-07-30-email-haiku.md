---
author: "James"
date: 2010-07-30T00:00:00Z
description: ""
draft: false
slug: "email-haiku"
aliases:
  - /email-haiku
title: "Email haiku"

---

I reserve the copyright to any comment haikus which may appear in my code. It says that in my contract, right? ;-)

    function sendEmail($email, $contents) {
    	/* AN EMAIL IS SENT
    	BRUSHES MAIL SERVER LIKE LEAF
    	SOON USER RECEIVES */


---
author: "James"
categories: ["humor", "politics", "conspiracy"]
date: 2013-10-19T03:02:27Z
description: ""
draft: false
slug: "the-2013-us-government-shutdown-conspiracy"
aliases:
  - /the-2013-us-government-shutdown-conspiracy
tags: ["humor", "politics", "conspiracy"]
title: "The 2013 US Government Shutdown Conspiracy"

---

A meme currently making the rounds is that the Republican party was been conspiring to completely shut down the government for months. It was never the backup plan, it was the only plan. The primary problem with this theory is that is is boring. BORING. You want a conspiracy theory, you go all in. It was *obviously* the Illuminati.

Are you familiar with this group? Let me fill you in. They control:

- Obama
- The Senate
- The UDSA
- Scientific journals
- Celeberties
- The Pope, who is apparently a Satanist.
- Dentists
- The House of Representatives. *Or do they?*

Now, the Tea Party is allegedly responsible. A house divided against itself cannot stand; the conspiracy can't control both sides. Nor would they *need* to control both sides to get the current situation. So which side is compromised?

One option is that the Tea Partiers are under Illuminati control. In this case, the conspiracy is significantly more wimpy than previously reported. Their secret agenda is actually a grass-roots movement for smaller government! This doesn't make a vast amount of sense. There's no point in going to such great lengths to destroy the Republicans credibility with the media either; it wasn't great to begin with.

In the alternative scenario, the Tea Party is the only force that can stop the Illuminati's evil scheme to provide healthcare for everyone via dubious economic policy.

This is all nonsense; the Illuminati clearly had nothing to do with the shutdown. It was the Reptilians!


---
author: "James"
date: 2015-12-19T06:03:20Z
description: ""
draft: false
cover: "/images/2015/12/echo-1.jpg"
slug: "things-i-want-to-do-with-amazon-echo"
aliases:
  - /things-i-want-to-do-with-amazon-echo
tags: ["review", "gadget"]
title: "Things I want to do with Amazon Echo"
---

My employer gave me an [Amazon Echo](http://smile.amazon.com/Amazon-SK705DI-Echo/dp/B00X4WHP5E) for Christmas (said employer is pretty rad!). The Echo is basically a standalone version of Apple's Siri. Of course, I promptly tried to get to do everything, with varying levels of success.

My initial concern was privacy. I haven't sniffed wifi traffic yet, but it doesn't appear to be transmitting all noise in the room to Amazon at all times; it only activates when it hears it's name. If you say, "Play some music, Alexa," the blue ring of light that indicates that it's listening only turns on at the end of the sentence; commands must be preceded with the "wake word."

Here are the things I've tried to do with my Echo:

#### Change the unit name (wake word)
By default, you may call it "Alexa" or "Amazon." I'd like to rename it to "Hal," "Echo-chan," or "Computer," but that's not an option right now.

#### Integrate with Google Play Music
Google's music service isn't supported, possibly because it doesn't have an official API. I can manually pair my phone with the Echo and stream music over bluetooth, but that's fiddly.

#### Resume podcasts
I use Overcast in the car and often come home from work with ten to twenty minutes left in an episode. Overcast has some cloud component; it'd be neat if I could walk into the house and say "Alexa, resume podcast."

Again, bluetooth connection to my phone. Unfortunately, this leads me to the next item...

#### Easily switch between paired bluetooth devices
The Echo is a better speaker than my phone or my laptop, so I wanted to be able to easily switch between the two, perhaps by saying "Alexa, pair with my laptop," or "Alexa, pair with my phone." What actually ends up happening is fiddling with the bluetooth and audio output control panels on the computer whenever I want to switch. At the very least, I'm going to need to automate what I'm doing on the computer.

#### Sync the todo list with my phone
The Echo allows you to record and update todo lists by voice. It'd be super cool if this synced with the iOS Reminders app via iCloud. The Echo integrates with [IFTTT](https://ifttt.com/) and has todo list change events, but iCloud isn't on the list of supported ITTT services.

#### Read Google Now cards
Google has no API to make this possible. [Chrome had a half-baked implementation](http://googlesystem.blogspot.com/2014/01/google-now-in-chrome-for-desktop.html) which seems to have recently vanished with the much-maligned bell icon.

#### Notifications when somebody mentions me in IRC or Slack
Ideally, this would also let me say something in response. But the Echo is currently a pull-only device; Amazon appears to be in no hurry to emulate Android's notification avalanche. Possibly for the best.

#### Use as a bluetooth mic
The Echo has a pretty amazing mic; it can pick me up and understand what I'm saying from across the room. This doesn't appear to be a thing right now, though.

#### Turn on the lights without paying a hundred bucks or two
None of my home appliances are internet-of-things things because internet-of-things things are computers and computers are inherently malicious little beasts that like to have major unpatched security vulnerabilities and mine bitcoin for the mob.

I'm starting from scratch here, and I'd like to have scratch left over. I only have one bulb that really needs voice activation; one where I have to walk halfway across the room in the dark to reach the switch.

Fifteen bucks a pop for a smart LED lightbulb is a significant price jump from what a CFL bulb costs, but LEDs last forever, so I can dig it. The $90 hub it requires is a harder sell, especially for a single bulb. Another alternative is, Emberlight which is a $50 socket that accommodates filthy commoner light bulbs. The Echo even talks to it! But they're sold out and won't have more for a month.

#### Netflix mode
I want to say "Alexa, netflix," and have the TV turn itself on and switch to the Chromecast input. Meanwhile, my iPhone has already opened the Netflix app. But wait, that would require devices from Amazon, Google, and Apple to be slightly compatible with each other. Ha ha! What was I thinking?

#### Do as much of this as possible without hitting Amazon's servers
This seems pretty unlikely.

#### TL;DR
The Echo is really cool, and I want it to talk to every computer in my house. It currently talks to none of them. It has an [SDK](https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit), and brief googling shows that some people have kinda reverse engineered the way it talks to Amazon. Either I write a bunch of code, or Amazon gets some decent third-party service integration with direct competitors. A conundrum!

---
author: "James"
categories: ["writing", "stories"]
date: 2015-05-12T03:33:01Z
description: ""
draft: true
slug: "how-agent-carter-should-have-ended"
aliases:
  - /how-agent-carter-should-have-ended
tags: ["writing", "stories"]
title: "How Agent Carter Should Have Ended"

---

Agent Carter was great, but I found the grand finale unsatisfactory. Our revisions pick up in the final episode.

- Everything is more or less the same until Stark is kidnapped.
- The case is at a dead end. Nobody can find Stark anywhere.
- Sousa remembers some snarky comment Stark made earlier about the VE day celebration in Times Square.
- Of course! They're going to dose the crowd with Midnight Oil.
- Peggy and agents arrive in Times Square and spread out to locate Underwood and Fennhoff.
- We can keep the awesome scene where Sousa uses earplugs to apprehend Fennhoff.
- Peggy acts on a hunch and climbs onto the roof of a building. She sees Stark on a nearby roof tinkering with the dispersion mechanism.
- Peggy breaks into a run and calls out to Stark; tries to talk him away from the mechanism.
- It's Underwood! Girl fight happens. Stark tries to explain that the device will melt the ice; that he's working to bring back Steve. He's clearly hypnotized and doesn't understand what he's really doing.
- Carter knocks Underwood unconscious. Stark is still doing stuff.
- We're getting annoyed at Stark. It's been too long since Carter punched him out, and now he's going to murder half the city.
- He's reaching for the lever and she's not going to make it in time. Carter grabs a brick and hurls it at Stark, knocking him on the head. He falls backwards, his hand catching the lever on the dispersion mechanism. Midnight Oil starts gently spraying out over the edge of the rooftop in a fine mist. Observant viewers will notice that Underwood is no longer on the ground behind Peggy.
- Peggy lunges forward and jumps between roofs. Underwood suddenly appears in front of her!
- Underwood pulls out a small sprayer and empties it in Carter's face. It's more Midnight Oil!
- Carter plows through the mist, catching Underwood off guard. She can grunt something about being tired of being manipulated as she punches Underwood off the edge of the building.
- Gasping for air, she turns off the dispersion machine and slumps to her hands and knees. She looks over the edge of the roof. The mist is slowly drifting downwards, towards the revelers. She's failed!
- "Rogers." Stark is lying on the floor, half dazed. "Midnight Oil came out of the Project Rebirth program that created Rogers. His blood could be an antidote."
- Carter doesn't like it, but she puts the vial of Roger's blood she's been carrying into the machine.
- Carter says goodbye to her true love one last time as his blood saves the city.
- _[Episode 8's concluding scenes happen as usual, sans Carter pouring blood into the river. Carter can explain to Stark that she simply held her breath when Underwood tried to dose her with Midnight Oil]_

#### What this fixes
Captain America's blood drives a huge part of the story, but it didn't play a part in conclusion of the original script. It was a MacGuffin that ultimately got unsatisfyingly dumped into the river.

#### Issues
We still need a compelling reason why the agents are so late to show up to the scene of the crime-in-progress. We also still need Stark to be kidnapped. These are weak spots in both my revision and the original show.

Captain America's blood as a magic cure-all seems like a thing that might be problematic for stories in the future where Cap is alive again. It's the same problem _Star Trek: Into Darkness_ has with <ins style="background-color:black">Kahn's blood</ins>.

We've already had a vivid depiction of what Midnight Oil can do. Does making Carter seemingly immune to it break suspension of disbelief?

Midnight Oil is slowly floating towards the crowd below. Cap's blood must be used quickly for the audience to buy that it can stop the effects of Midnight Oil before things get out of hand. But the decision is very important to Carter and needs to be given appropriate weight. These two storytelling goals are at odds.

Does Carter still need a quiet scene alone to get closure on Rogers?

